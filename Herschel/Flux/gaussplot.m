function gaussplot(fitmodel, Coeffs, X) %#ok<INUSD,INUSL>

ngauss = (length(Coeffs)-2)/3;
for i=1:ngauss
    eval(['str' num2str(i) ' = '''';'])
end
for j=1:length(Coeffs)
    for i=1:ngauss
        if j==i
            eval(['str' num2str(i) ' = strcat(str' num2str(i) ...
                ',''Coeffs(' num2str(i) ')'','','');'])
        elseif j==ngauss+2+i
            eval(['str' num2str(i) ' = strcat(str' num2str(i) ...
                ',''Coeffs(' num2str(j) ')'','','');'])
        elseif j==2*ngauss+2+i
            eval(['str' num2str(i) ' = strcat(str' num2str(i) ...
                ',''Coeffs(' num2str(j) ')'','','');'])
        else
            eval(['str' num2str(i) ' = strcat(str' num2str(i) ...
                ',num2str(0),'','');'])
        end
    end
end
figure
set(gcf,'units','normalized')
set(gcf,'position',[0 0 1 1])
plotstr = '';
lstr = cell(ngauss,1);
for i=1:ngauss
    eval(['str' num2str(i) '(end) = [];'])
    command = eval(['strcat(''gauss' num2str(i) ' = fitmodel('',str' ...
        num2str(i) ','',X);'')']);
    eval(command);
    plotstr = strcat(plotstr,'X',',',['gauss' num2str(i)],',');
    lstr{i} = num2str(i);
end
plotstr(end) = [];
eval(['plot(' plotstr ')'])
legend(lstr)
fs = 12;
set(gca,'fontsize',fs)

end