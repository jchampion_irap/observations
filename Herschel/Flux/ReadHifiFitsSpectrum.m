function [freq,Tantenna] = ReadHifiFitsSpectrum(filepath,bandindex)

data = fitsread(filepath,'binarytable');
freq = data{4*bandindex};
Tantenna = data{1+4*(bandindex-1)};

end