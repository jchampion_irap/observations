function [X, spectrum] = MeanSpectrum(X1, spectrum1, X2, spectrum2)

spectrum1 = spectrum1(:);
spectrum2 = spectrum2(:);
X1 = X1(:);
X2 = X2(:);
if isequal(X1,X2)
    X = X1;
    spectrum = mean([spectrum1 spectrum2],2);
else
    lim_inf = max([min(X1) min(X2)]);
    lim_sup = min([max(X1) max(X2)]);
    nval1 = length(X1(X1>=lim_inf & X1<=lim_sup));
    nval2 = length(X2(X2>=lim_inf & X2<=lim_sup));
    nval = fix(2.5*max([nval1 nval2]));
    X = linspace(lim_inf,lim_sup,nval);
    X = X(:);
    spectrum1_interp = interp1(X1,spectrum1,X);
    spectrum1_interp = spectrum1_interp(:);
    spectrum2_interp = interp1(X2,spectrum2,X);
    spectrum2_interp = spectrum2_interp(:);
    spectrum = mean([spectrum1_interp spectrum2_interp],2);
end

end