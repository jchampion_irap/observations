function [xt, xtl, xt_anc, xtl_anc] = XSpeed(dvc,f0,fc,fs)

c = 299792458; % Light velocity (m/s).
vc = (fc-f0)/f0*c;
dv = dvc + vc;
df = dv/c*f0;
f = f0+df;

xt = f;
xtl = num2str(1e-3*dvc(:));

xtl_anc = get(gca,'XTickLabel');
xt_anc = get(gca,'XTick');
set(gca,'XTick',xt)
set(gca,'XTickLabel',xtl)
xlabel('Velocity (km/s)','fontsize',fs)

end