%% Cleaning workspace.
clear all
close all
clc

%% OI Carina
% Constant definition
c = 299792458;

% Get data from the FITS file.
path = '../Spectra/';
file = 'CARINA_OI_PACSCube.fits';
filepath = [path file];
cube = fitsread(filepath,'image');
l = fitsread(filepath,'binarytable');
l = l{1}*1e-6;
freq = c./l;
freq_Ghz = freq*1E-9;

figure
set(gcf,'units','normalized')
set(gcf,'position',[0 0 1 1])
ind = length(l)/4:3*length(l)/4;
freq_Ghz = freq_Ghz(ind);
for i=1:5
    for j=1:5
        subplot(5,5,(i-1)*5+j)
        sp = squeeze(cube(i,j,:));
        sp = sp(ind);
        plot(freq_Ghz,sp)
    end
end

% figure
% set(gcf,'units','normalized')
% set(gcf,'position',[0 0 1 1])
cont = zeros(size(cube,1),size(cube,2),length(freq_Ghz));
for i=1:5
    for j=1:5
        hold on
        sp = squeeze(cube(i,j,:));
        sp = sp(ind);
        gin = [4743.92741935484;4746.86290322581];
        [fitresult, fitmodel] = gaussfit(freq_Ghz,sp,1,1,...
            'nointeraction',0,gin);
        cont(i,j,:) = fitresult.a0+fitresult.a1*freq_Ghz;
    end
end

close all
figure
set(gcf,'units','normalized')
set(gcf,'position',[0 0 1 1])
for i=1:5
    for j=1:5
        subplot(5,5,(i-1)*5+j)
        sp = squeeze(cube(i,j,:));
        sp = sp(ind);
        contij = squeeze(cont(i,j,:));
        plot(freq_Ghz,sp,'b.-',freq_Ghz,contij,'r');
    end
end

contc = squeeze(cont(3,3,:));
context = zeros(size(contc));
for i=1:5
    for j=1:5
        if i==1 || i==5 || j==1 || j==5
            context = context + squeeze(cont(i,j,:));
        end
    end
end
context = context/16;
cont_corr = contc-context;
figure
set(gcf,'units','normalized')
set(gcf,'position',[0 0 1 1])
plot(freq_Ghz,contc,'b.-',freq_Ghz,context,'r.-',freq_Ghz,cont_corr,'k')
legend('Central spaxel continuum','Outer spaxels average continuum',...
    'Difference')

pix = (9.4/3600 * pi/180)^2;
intcont = mean(cont_corr(:));
stdcont = std(cont_corr(:));
disp(['Carina OI : Cont = ' num2str(intcont) ' +- ' num2str(stdcont) ...
    ' Jy/pixel @ ' num2str(mean(l)*1E6) ' micrometers']);
intcontPerSr = intcont*1E-6 / pix; %MJy/sr
stdcontPerSr = stdcont*1E-6 / pix;
disp(['Carina OI : Cont = ' num2str(intcontPerSr) ' +- ' ...
    num2str(stdcontPerSr) ...
    ' MJy/sr @ ' num2str(mean(l)*1E6) ' micrometers']);

%% CO 19-18 Carina
% Constant definition
c = 299792458;

% Get data from the FITS file.
path = '../Spectra/';
file = 'CARINA_CO_PACSCube_137um.fits';
filepath = [path file];
cube = fitsread(filepath,'image');
l = fitsread(filepath,'binarytable',4);
l = l{1}*1e-6;
freq = c./l;
freq_Ghz = freq*1E-9;

figure
set(gcf,'units','normalized')
set(gcf,'position',[0 0 1 1])
ind = length(l)/4:3*length(l)/4;
freq_Ghz = freq_Ghz(ind);
for i=1:5
    for j=1:5
        subplot(5,5,(i-1)*5+j)
        sp = squeeze(cube(i,j,:));
        sp = sp(ind);
        plot(freq_Ghz,sp)
    end
end

% figure
% set(gcf,'units','normalized')
% set(gcf,'position',[0 0 1 1])
cont = zeros(size(cube,1),size(cube,2),length(freq_Ghz));
for i=1:5
    for j=1:5
        hold on
        sp = squeeze(cube(i,j,:));
        sp = sp(ind);
        [fitresult, fitmodel] = gaussfit(freq_Ghz,sp,0);
        cont(i,j,:) = fitresult.a0+fitresult.a1*freq_Ghz;
    end
end

close all
figure
set(gcf,'units','normalized')
set(gcf,'position',[0 0 1 1])
for i=1:5
    for j=1:5
        subplot(5,5,(i-1)*5+j)
        sp = squeeze(cube(i,j,:));
        sp = sp(ind);
        contij = squeeze(cont(i,j,:));
        plot(freq_Ghz,sp,'b.-',freq_Ghz,contij,'r');
    end
end

contc = squeeze(cont(3,3,:));
context = zeros(size(contc));
for i=1:5
    for j=1:5
        if i==1 || i==5 || j==1 || j==5
            context = context + squeeze(cont(i,j,:));
        end
    end
end
context = context/16;
cont_corr = contc-context;
figure
set(gcf,'units','normalized')
set(gcf,'position',[0 0 1 1])
plot(freq_Ghz,contc,'b.-',freq_Ghz,context,'r.-',freq_Ghz,cont_corr,'k')
legend('Central spaxel continuum','Outer spaxels average continuum',...
    'Difference')

pix = (9.4/3600 * pi/180)^2;
intcont = mean(cont_corr(:));
stdcont = std(cont_corr(:));
disp(['Carina CO 19-18 : Cont = ' num2str(intcont) ' +- ' num2str(stdcont) ...
    ' Jy/pixel @ ' num2str(mean(l)*1E6) ' micrometers']);
intcontPerSr = intcont*1E-6 / pix; %MJy/sr
stdcontPerSr = stdcont*1E-6 / pix;
disp(['Carina CO 19-18 : Cont = ' num2str(intcontPerSr) ' +- ' ...
    num2str(stdcontPerSr) ...
    ' MJy/sr @ ' num2str(mean(l)*1E6) ' micrometers']);

%% CO 17-16 Carina
% Constant definition
c = 299792458;

% Get data from the FITS file.
path = '../Spectra/';
file = 'CARINA_CO_PACSCube_153um.fits';
filepath = [path file];
cube = fitsread(filepath,'image');
l = fitsread(filepath,'binarytable',4);
l = l{1}*1e-6;
freq = c./l;
freq_Ghz = freq*1E-9;

figure
set(gcf,'units','normalized')
set(gcf,'position',[0 0 1 1])
ind = length(l)/4:3*length(l)/4;
freq_Ghz = freq_Ghz(ind);
for i=1:5
    for j=1:5
        subplot(5,5,(i-1)*5+j)
        sp = squeeze(cube(i,j,:));
        sp = sp(ind);
        plot(freq_Ghz,sp)
    end
end

% figure
% set(gcf,'units','normalized')
% set(gcf,'position',[0 0 1 1])
cont = zeros(size(cube,1),size(cube,2),length(freq_Ghz));
for i=1:5
    for j=1:5
        hold on
        sp = squeeze(cube(i,j,:));
        sp = sp(ind);
        gin = [1953.69153225806;1957.64314516129];
        [fitresult, fitmodel] = gaussfit(freq_Ghz,sp,0,0,...
            'nointeraction',0,gin);
        cont(i,j,:) = fitresult.a0+fitresult.a1*freq_Ghz;
    end
end

close all
figure
set(gcf,'units','normalized')
set(gcf,'position',[0 0 1 1])
for i=1:5
    for j=1:5
        subplot(5,5,(i-1)*5+j)
        sp = squeeze(cube(i,j,:));
        sp = sp(ind);
        contij = squeeze(cont(i,j,:));
        plot(freq_Ghz,sp,'b.-',freq_Ghz,contij,'r');
    end
end

contc = squeeze(cont(3,3,:));
context = zeros(size(contc));
for i=1:5
    for j=1:5
        if i==1 || i==5 || j==1 || j==5
            context = context + squeeze(cont(i,j,:));
        end
    end
end
context = context/16;
cont_corr = contc-context;
figure
set(gcf,'units','normalized')
set(gcf,'position',[0 0 1 1])
plot(freq_Ghz,contc,'b.-',freq_Ghz,context,'r.-',freq_Ghz,cont_corr,'k')
legend('Central spaxel continuum','Outer spaxels average continuum',...
    'Difference')

pix = (9.4/3600 * pi/180)^2;
intcont = mean(cont_corr(:));
stdcont = std(cont_corr(:));
disp(['Carina CO 17-16 : Cont = ' num2str(intcont) ' +- ' num2str(stdcont) ...
    ' Jy/pixel @ ' num2str(mean(l)*1E6) ' micrometers']);
intcontPerSr = intcont*1E-6 / pix; %MJy/sr
stdcontPerSr = stdcont*1E-6 / pix;
disp(['Carina CO 17-16 : Cont = ' num2str(intcontPerSr) ' +- ' ...
    num2str(stdcontPerSr) ...
    ' MJy/sr @ ' num2str(mean(l)*1E6) ' micrometers']);

%% CO 15-14 Carina
% Constant definition
c = 299792458;

% Get data from the FITS file.
path = '../Spectra/';
file = 'CARINA_CO_PACSCube_173um.fits';
filepath = [path file];
cube = fitsread(filepath,'image');
l = fitsread(filepath,'binarytable',4);
l = l{1}*1e-6;
freq = c./l;
freq_Ghz = freq*1E-9;

figure
set(gcf,'units','normalized')
set(gcf,'position',[0 0 1 1])
ind = length(l)/4:3*length(l)/4;
freq_Ghz = freq_Ghz(ind);
for i=1:5
    for j=1:5
        subplot(5,5,(i-1)*5+j)
        sp = squeeze(cube(i,j,:));
        sp = sp(ind);
        plot(freq_Ghz,sp)
    end
end

% figure
% set(gcf,'units','normalized')
% set(gcf,'position',[0 0 1 1])
cont = zeros(size(cube,1),size(cube,2),length(freq_Ghz));
for i=1:5
    for j=1:5
        hold on
        sp = squeeze(cube(i,j,:));
        sp = sp(ind);
        gin = [1725.00806451613;1728.45967741935];
        [fitresult, fitmodel] = gaussfit(freq_Ghz,sp,0,0,...
            'nointeraction',0,gin);
        cont(i,j,:) = fitresult.a0+fitresult.a1*freq_Ghz;
    end
end

close all
figure
set(gcf,'units','normalized')
set(gcf,'position',[0 0 1 1])
for i=1:5
    for j=1:5
        subplot(5,5,(i-1)*5+j)
        sp = squeeze(cube(i,j,:));
        sp = sp(ind);
        contij = squeeze(cont(i,j,:));
        plot(freq_Ghz,sp,'b.-',freq_Ghz,contij,'r');
    end
end

contc = squeeze(cont(3,3,:));
context = zeros(size(contc));
for i=1:5
    for j=1:5
        if i==1 || i==5 || j==1 || j==5
            context = context + squeeze(cont(i,j,:));
        end
    end
end
context = context/16;
cont_corr = contc-context;
figure
set(gcf,'units','normalized')
set(gcf,'position',[0 0 1 1])
plot(freq_Ghz,contc,'b.-',freq_Ghz,context,'r.-',freq_Ghz,cont_corr,'k')
legend('Central spaxel continuum','Outer spaxels average continuum',...
    'Difference')

pix = (9.4/3600 * pi/180)^2;
intcont = mean(cont_corr(:));
stdcont = std(cont_corr(:));
disp(['Carina CO 15-14 : Cont = ' num2str(intcont) ' +- ' num2str(stdcont) ...
    ' Jy/pixel @ ' num2str(mean(l)*1E6) ' micrometers']);
intcontPerSr = intcont*1E-6 / pix; %MJy/sr
stdcontPerSr = stdcont*1E-6 / pix;
disp(['Carina CO 15-14 : Cont = ' num2str(intcontPerSr) ' +- ' ...
    num2str(stdcontPerSr) ...
    ' MJy/sr @ ' num2str(mean(l)*1E6) ' micrometers']);