function [CubeCont, CentralContCorr, MeanCentralCont, dMeanCentralContRel] = CubeContinuum(x,Cube,gin,figdisplay,NoiseThreshold)

ind = fix(length(x)/4):ceil(3*length(x)/4);
x = x(ind);

CubeCont = zeros(size(Cube,1),size(Cube,2),length(x));
for i=1:5
    for j=1:5
        sp = squeeze(Cube(i,j,:));
        sp = sp(ind);
        [fitresult, fitmodel] = gaussfit(x,sp,1,1,...
            'nointeraction',0,gin);
        C = coeffvalues(fitresult);
        int_noise = sp - fitmodel(C(1),C(2),C(3),C(4),C(5),x);
        Noise = NoiseEstimation(x,int_noise,1,0);
        % If no emission lines detected, fit a straight line with no
        % gaussian functions.
        if ~exist('NoiseThreshold','var')
            NoiseThreshold = 2;
        end
        if fitresult.A1/Noise < NoiseThreshold
            [fitresult, ~] = gaussfit(x,sp,0,0,...
            'nointeraction',0,gin);
        end
        CubeCont(i,j,:) = fitresult.a0+fitresult.a1*x;
    end
end

close all
if figdisplay == 1
    figure
    set(gcf,'units','normalized')
    set(gcf,'position',[0 0 1 1])
end
for i=1:5
    for j=1:5
        sp = squeeze(Cube(i,j,:));
        sp = sp(ind);
        contij = squeeze(CubeCont(i,j,:));
        if figdisplay == 1
            subplot(5,5,(i-1)*5+j)
            plot(x,sp,'b.-',x,contij,'r');
        end
    end
end

contc = squeeze(CubeCont(3,3,:));
context = zeros(size(contc));
for i=1:5
    for j=1:5
        if i==1 || i==5 || j==1 || j==5
            context = context + squeeze(CubeCont(i,j,:));
        end
    end
end
% indext_i = [1 1 1 1 1 5 5 5 5 5 2 3 4 2 3 4];
% indext_j = [1 2 3 4 5 1 2 3 4 5 1 1 1 5 5 5];

context = context/16;
CentralContCorr = contc-context;

if figdisplay == 1
    figure
    set(gcf,'units','normalized')
    set(gcf,'position',[0 0 1 1])
    plot(x,contc,'b.-',x,context,'r.-',x,CentralContCorr,'k')
    legend('Central spaxel continuum','Outer spaxels average continuum',...
        'Difference')
end

MeanCentralCont = mean(CentralContCorr(:));
ContUncert = 10; % Relative flux calibration accuracies between spaxels (in
% percent).
% dMeanCentralContRel = sqrt(ContUncert^2 + ContUncert^2/16);
MeanCubeCont = mean(CubeCont,3);
indext = [1:6 10 11 15 16 20:25];
meancontext = mean(MeanCubeCont(indext));
stdcontRel = 100*(abs(std(MeanCubeCont(indext))-meancontext)/abs(meancontext));
dMeanCentralContRel = sqrt(ContUncert^2 + stdcontRel^2);

end