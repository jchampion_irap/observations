%% Workspace cleaning
clear all
close all
clc

%% Constant parameters
k = 1.3806488E-23; % Boltzmann constant (J/K).
c = 299792458; % Light velocity (m/s).
f0 = 806.65181; % Line frequency (Ghz).
HIFIBand = 3; %  Band of HIFI used in the observation.

% Parameters for the output data file.
Instru = 'HIFI';
Line = 'CO 7-6';
IndLine = 2;
load('LineFluxes.mat')

% Wavelength and FWHM.
wvl = c*1e6/(f0*1e9); % Wavelength in micrometers.
Table = [480 44.3; 640 33.2; 800 26.6; 960 22.2; 1120 19; 1410 15.2;...
    1910 11.2]; % Frequency (GHz) vs FWHM (arcsec.), from HIFI observer manual.
% Calculation of the FWHM (frequency dependent).
[~, indi] = min(abs(Table(:,1)-f0));
FWHM = Table(indi,2) * Table(indi,1)/f0; % Because f1/f2 = FWHM2/FWHM1.
clear('Table','indi')

%% CO 7-6 Line flux : Carina
% Load data from the FITS file.
path = '../Spectra/';
file = 'CARINA_CO76_HIFI_WBS_H_LSB.fits';
filepath = [path file];
[freqH,TantennaH] = ReadHifiFitsSpectrum(filepath,4);
file = 'CARINA_CO76_HIFI_WBS_V_LSB.fits';
filepath = [path file];
[freqV,TantennaV] = ReadHifiFitsSpectrum(filepath,4);
% Mean spectrum.
[freq, Tantenna] = MeanSpectrum(freqH, TantennaH, freqV, TantennaV);
clear('path','file','filepath','freqH','TantennaH','freqV','TantennaV')

% Fit.
gin =[806.697883064516;806.715;806.717641129032;806.730342741936];
[fitresult, fitmodel] = gaussfit(freq,Tantenna,2,1,'nointeraction',0,gin);
C = coeffvalues(fitresult);
gaussplot(fitmodel, C, freq)
clear('gin')

% Integrated flux.
Cint = confint(fitresult);
dC = Cint(2,:)-C;
[Flux, dFluxRel] = GaussT2Flux(C(1),C(7)*1E9,f0*1E9,dC(1),dC(7)*1E9,...
    HIFIBand);
dFluxRel = sqrt(dFluxRel^2 + GetHIFIUncert(HIFIBand)^2);
dFlux = Flux*dFluxRel/100;
disp(['Carina : CO 7-6 Line flux = ' num2str(Flux) ' +- ' num2str(dFlux) ...
    ' (' num2str(ceil(100*dFluxRel)/100) '%) W/m^2/sr']);

% Save data to output data file.
Object = 'Carina';
ValueType = 'Flux';
LineFluxes(1+3*(IndLine-1)+1,:) = {Instru,Line,wvl,FWHM,Object,ValueType,Flux,dFluxRel};
clear('Object','ValueType')
save('LineFluxes.mat','LineFluxes')

% Line width used to estimate detection limit for the Orion Proplyds.
std_gauss_signal = C(7);
std_gauss_signal_err = dC(7);
clear('C','Cint','dC')

%% CO 7-6 Line flux : HST 10
% Load data from the FITS file.
path = '../Spectra/';
file = 'HST10_CO76_HIFI_WBS_H_LSB.fits';
filepath = [path file];
[freqH,TantennaH] = ReadHifiFitsSpectrum(filepath,4);
file = 'HST10_CO76_HIFI_WBS_V_LSB.fits';
filepath = [path file];
[freqV,TantennaV] = ReadHifiFitsSpectrum(filepath,4);
% Mean spectrum.
[freq, Tantenna] = MeanSpectrum(freqH, TantennaH, freqV, TantennaV);
clear('path','file','filepath','freqH','TantennaH','freqV','TantennaV')

% Noise calculations.
dv=-26000;
f0doppler = f0+dv*f0/c;
close all; 
freqlim = [806.55 806.6]; % Select a portion of the spectrum, near the expected but undetected line.
index = freq>freqlim(1) & freq<freqlim(2);
freq = freq(index);
Tantenna = Tantenna(index);
Noise = NoiseEstimation(freq,Tantenna,3,0,f0doppler);
[minflux, dminfluxRel] = GaussT2Flux(2*Noise,std_gauss_signal*1E9,f0*1E9,0,...
    std_gauss_signal_err*1E9,HIFIBand);
dminfluxRel = sqrt(dminfluxRel^2 + GetHIFIUncert(HIFIBand)^2);
dminflux = minflux*dminfluxRel/100;
disp(['HST 10 : CO 7-6 Line flux < ' num2str(minflux) ' +- ' num2str(dminflux) ...
    ' (' num2str(ceil(100*dminfluxRel)/100) '%) W/m^2/sr ' char(10) ...
    '(For an intensity equals to 2 * noise standard deviation and a ' ...
    'line width assessed from the detection on the Carina Proplyd.)']);
clear('dv','f0doppler','freqlim','index','dminflux')

% Save data to output data file.
Object = 'HST 10';
ValueType = 'SupFlux';
LineFluxes(1+3*(IndLine-1)+2,:) = {Instru,Line,wvl,FWHM,Object,ValueType,minflux,dminfluxRel};
clear('Object','ValueType')
save('LineFluxes.mat','LineFluxes')

%% CO 7-6 Line flux : 244-440
% Load data from the FITS file.
path = '../Spectra/';
file = 'H244_440_CO76_HIFI_WBS_H_LSB.fits';
filepath = [path file];
[freqH,TantennaH] = ReadHifiFitsSpectrum(filepath,4);
file = 'H244_440_CO76_HIFI_WBS_V_LSB.fits';
filepath = [path file];
[freqV,TantennaV] = ReadHifiFitsSpectrum(filepath,4);
% Mean spectrum.
[freq, Tantenna] = MeanSpectrum(freqH, TantennaH, freqV, TantennaV);
clear('path','file','filepath','freqH','TantennaH','freqV','TantennaV')

% Noise calculations.
dv=-25000; % See Vicente poster or Henney & O'Dell (1999).
f0doppler = f0+dv*f0/c;
close all;
freqlim = [806.55 806.6];
index = freq>freqlim(1) & freq<freqlim(2);
freq = freq(index);
Tantenna = Tantenna(index);
Noise = NoiseEstimation(freq,Tantenna,2,0,f0doppler);
[minflux, dminfluxRel] = GaussT2Flux(2*Noise,std_gauss_signal*1E9,f0*1E9,0,...
    std_gauss_signal_err*1E9,HIFIBand);
dminfluxRel = sqrt(dminfluxRel^2 + GetHIFIUncert(HIFIBand)^2);
dminflux = minflux*dminfluxRel/100;
disp(['244-440 : CO 7-6 Line flux < ' num2str(minflux) ' +- ' num2str(dminflux) ...
    ' (' num2str(ceil(100*dminfluxRel)/100) '%) W/m^2/sr ' char(10) ...
    '(For an intensity equals to 2 * noise standard deviation and a ' ...
    'line width assessed from the detection on the Carina Proplyd.)']);
clear('dv','f0doppler','freqlim','index','dminflux')

% Save data to output data file.
Object = '244-440';
ValueType = 'SupFlux';
LineFluxes(1+3*(IndLine-1)+3,:) = {Instru,Line,wvl,FWHM,Object,ValueType,minflux,dminfluxRel};
clear('Object','ValueType')
save('LineFluxes.mat','LineFluxes')