%% Workspace cleaning
clear all
close all
clc

%% Constant parameters
k = 1.3806488E-23; % Boltzmann constant (J/K).
c = 299792458; % Light velocity (m/s).
NoiseThreshold = 3; %

% Parameters for the output data file.
nObs = 4;
CarinaContinuum = cell(nObs+1,3); % Wavelength (um) | Continuum (MJy/sr) | Uncertainty (%).
CarinaContinuum(1,:) = {'Wavelength (um)','Continuum (MJy/sr)','Uncertainty (%)'};

%% 173um Continuum : Carina
% Get data from the FITS file.
path = '../Spectra/';
file = 'CARINA_CO_PACSCube_173um.fits';
filepath = [path file];
cube = fitsread(filepath,'image');
l = fitsread(filepath,'binarytable',4);
l = l{1}*1e-6;
freq = c./l;
freq_Ghz = freq*1E-9;
clear('path','file','filepath')

% Get continuum in Jy/pixel.
gin = [1725.00806451613;1728.45967741935];
[CubeCont, ~, MeanCentralCont, dMeanCentralContRel] = ...
    CubeContinuum(freq_Ghz,cube,gin,1,NoiseThreshold);
dMeanCentralCont = MeanCentralCont * dMeanCentralContRel/100;
clear('gin')

% disp(['Carina : 173um continuum = ' num2str(MeanCentralCont) ' +- ' ...
%     num2str(dMeanCentralCont) ...
%     ' (' num2str(ceil(100*dMeanCentralContRel)/100)  ...
%     '%) Jy/pixel @ ' num2str(mean(l)*1E6) ' micrometers']);
pix = (9.4/3600 * pi/180)^2;
MeanCentralContPerSr = MeanCentralCont*1E-6 / pix; %MJy/sr
dMeanCentralContPerSr = dMeanCentralCont*1E-6 / pix;
disp(['Carina : 173um continuum = ' num2str(MeanCentralContPerSr) ...
    ' +- ' num2str(dMeanCentralContPerSr) ...
    ' (' num2str(ceil(100*dMeanCentralContRel)/100) ...
    '%) MJy/sr @ ' num2str(mean(l)*1E6) ' micrometers']);
clear('pix','MeanCentralCont','dMeanCentralCont','dMeanCentralContPerSr')

CarinaContinuum(2,:) = {mean(l)*1E6,MeanCentralContPerSr,dMeanCentralContRel};
save('CarinaContinuum.mat','CarinaContinuum')

CubeContMeanVal = abs(mean(CubeCont,3));
CubeContMeanVal_norm = CubeContMeanVal/max(CubeContMeanVal(:));
figure
imagesc(CubeContMeanVal_norm)
colorbar

%% 153um Continuum : Carina
% Get data from the FITS file.
path = '../Spectra/';
file = 'CARINA_CO_PACSCube_153um.fits';
filepath = [path file];
cube = fitsread(filepath,'image');
l = fitsread(filepath,'binarytable',4);
l = l{1}*1e-6;
freq = c./l;
freq_Ghz = freq*1E-9;
clear('path','file','filepath')

% Get continuum in Jy/pixel.
gin = [1955.09324596774;1957.18497983871];
[CubeCont, ~, MeanCentralCont, dMeanCentralContRel] = ...
    CubeContinuum(freq_Ghz,cube,gin,1,NoiseThreshold);
dMeanCentralCont = MeanCentralCont * dMeanCentralContRel/100;
clear('gin')

pix = (9.4/3600 * pi/180)^2;
MeanCentralContPerSr = MeanCentralCont*1E-6 / pix; %MJy/sr
dMeanCentralContPerSr = dMeanCentralCont*1E-6 / pix;
disp(['Carina : 153um continuum = ' num2str(MeanCentralContPerSr) ...
    ' +- ' num2str(dMeanCentralContPerSr) ...
    ' (' num2str(ceil(100*dMeanCentralContRel)/100) ...
    '%) MJy/sr @ ' num2str(mean(l)*1E6) ' micrometers']);
clear('pix','MeanCentralCont','dMeanCentralCont','dMeanCentralContPerSr')

CarinaContinuum(3,:) = {mean(l)*1E6,MeanCentralContPerSr,dMeanCentralContRel};
save('CarinaContinuum.mat','CarinaContinuum')

CubeContMeanVal = abs(mean(CubeCont,3));
CubeContMeanVal_norm = CubeContMeanVal/max(CubeContMeanVal(:));
figure
imagesc(CubeContMeanVal_norm)
colorbar

%% 137um Continuum : Carina
% Get data from the FITS file.
path = '../Spectra/';
file = 'CARINA_CO_PACSCube_137um.fits';
filepath = [path file];
cube = fitsread(filepath,'image');
l = fitsread(filepath,'binarytable',4);
l = l{1}*1e-6;
freq = c./l;
freq_Ghz = freq*1E-9;
clear('path','file','filepath')

% Get continuum in Jy/pixel.
gin = [2184;2187];
[CubeCont, ~, MeanCentralCont, dMeanCentralContRel] = ...
    CubeContinuum(freq_Ghz,cube,gin,1,NoiseThreshold);
dMeanCentralCont = MeanCentralCont * dMeanCentralContRel/100;
clear('gin')

pix = (9.4/3600 * pi/180)^2;
MeanCentralContPerSr = MeanCentralCont*1E-6 / pix; %MJy/sr
dMeanCentralContPerSr = dMeanCentralCont*1E-6 / pix;
disp(['Carina : 137um continuum = ' num2str(MeanCentralContPerSr) ...
    ' +- ' num2str(dMeanCentralContPerSr) ...
    ' (' num2str(ceil(100*dMeanCentralContRel)/100) ...
    '%) MJy/sr @ ' num2str(mean(l)*1E6) ' micrometers']);
clear('pix','MeanCentralCont','dMeanCentralCont','dMeanCentralContPerSr')

CarinaContinuum(4,:) = {mean(l)*1E6,MeanCentralContPerSr,dMeanCentralContRel};
save('CarinaContinuum.mat','CarinaContinuum')

CubeContMeanVal = abs(mean(CubeCont,3));
CubeContMeanVal_norm = CubeContMeanVal/max(CubeContMeanVal(:));
figure
imagesc(CubeContMeanVal_norm)
colorbar

%% 63um Continuum : Carina
% Get data from the FITS file.
path = '../Spectra/';
file = 'CARINA_OI_PACSCube.fits';
filepath = [path file];
cube = fitsread(filepath,'image');
l = fitsread(filepath,'binarytable',1);
l = l{1}*1e-6;
freq = c./l;
freq_Ghz = freq*1E-9;
clear('path','file','filepath')

% Get continuum in Jy/pixel.
gin = [4743.92741935484;4746.86290322581];
[CubeCont, ~, MeanCentralCont, dMeanCentralContRel] = ...
    CubeContinuum(freq_Ghz,cube,gin,1,NoiseThreshold);
dMeanCentralCont = MeanCentralCont * dMeanCentralContRel/100;
clear('gin')

pix = (9.4/3600 * pi/180)^2;
MeanCentralContPerSr = MeanCentralCont*1E-6 / pix; %MJy/sr
dMeanCentralContPerSr = dMeanCentralCont*1E-6 / pix;
disp(['Carina : 63um continuum = ' num2str(MeanCentralContPerSr) ...
    ' +- ' num2str(dMeanCentralContPerSr) ...
    ' (' num2str(ceil(100*dMeanCentralContRel)/100) ...
    '%) MJy/sr @ ' num2str(mean(l)*1E6) ' micrometers']);
clear('pix','MeanCentralCont','dMeanCentralCont','dMeanCentralContPerSr')

CarinaContinuum(5,:) = {mean(l)*1E6,MeanCentralContPerSr,dMeanCentralContRel};
save('CarinaContinuum.mat','CarinaContinuum')

CubeContMeanVal = abs(mean(CubeCont,3));
CubeContMeanVal_norm = CubeContMeanVal/max(CubeContMeanVal(:));
figure
imagesc(CubeContMeanVal_norm)
colorbar