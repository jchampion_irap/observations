%% Workspace cleaning
clear all
close all
clc

%% Constant parameters
k = 1.3806488E-23; % Boltzmann constant (J/K).
c = 299792458; % Light velocity (m/s).
f0 = 1900.5369; % Line frequency (Ghz).
HIFIBand = 7; %  Band of HIFI used in the observation.

% Parameters for the output data file.
Instru = 'HIFI';
Line = '[CII]';
IndLine = 1;
load('LineFluxes.mat')

% Wavelength and FWHM.
wvl = c*1e6/(f0*1e9); % Wavelength in micrometers.
Table = [480 44.3; 640 33.2; 800 26.6; 960 22.2; 1120 19; 1410 15.2;...
    1910 11.2]; % Frequency (GHz) vs FWHM (arcsec.), from HIFI observer manual.
% Calculation of the FWHM (frequency dependent).
[~, indi] = min(abs(Table(:,1)-f0));
FWHM = Table(indi,2) * Table(indi,1)/f0; % Because f1/f2 = FWHM2/FWHM1.
clear('Table','indi')

%% [CII] Line flux : Carina
% Load data from the FITS file.
path = '../Spectra/';
file = 'CARINA_CII_HIFI_WBS_H_USB.fits';
filepath = [path file];
[freqH,TantennaH] = ReadHifiFitsSpectrum(filepath,3);
file = 'CARINA_CII_HIFI_WBS_V_USB.fits';
filepath = [path file];
[freqV,TantennaV] = ReadHifiFitsSpectrum(filepath,3);
% Calculate the mean spectrum (with interpolation on a new grid).
[freq, Tantenna] = MeanSpectrum(freqH, TantennaH, freqV, TantennaV);
clear('path','file','filepath','freqH','TantennaH','freqV','TantennaV')

% Constraint bounds.
mp = 1.67262178E-27; % Proton mass.
mc = mp*12.01074/1.002765; % Atomic carbon mass.
T = 10E4; % Temperature of the nebula.
Tmin = 9E4;
Tmax = 11E4;
sigHII = sqrt(k*T/(mc*c^2))*f0; % Expected standard deviation on the nebula line.
sigminHII = sqrt(k*Tmin/(mc*c^2))*f0;
sigmaxHII = sqrt(k*Tmax/(mc*c^2))*f0;
VLSR = [-22.7 -22.6 -22.7 -22.6 -22.3 -22.6 -22.8]*1E3; % Measured velocity of the proplyd from Sahai's paper.
VLSR_m = mean(VLSR);
VLSR_s = std(VLSR);
df=-VLSR_m*f0/c; % Doppler effect caused by the proplyd movement.
dfmin=(-VLSR_m-VLSR_s)*f0/c;
dfmax=(-VLSR_m+VLSR_s)*f0/c;

% Fit.
gin = [1900.47953629032;1900.58397177419;1900.60090725806;...
    1900.72086693548;1900.65453629032;1900.70110887097;...
    1900.50917338710;1900.82671370968];
[fitresult, fitmodel] = gaussfit(freq,Tantenna,4,3,'stdlow',4,sigminHII,...
    'stdup',4,sigmaxHII,'stdstart',4,sigHII,'meanlow',2,f0+dfmin,...
    'meanup',2,f0+dfmax,'meanstart',2,f0+df,'samemean',3,2,...
    'nointeraction',0,gin);
clear('gin','mp','mc','T','Tmin','Tmax','sigHII','sigminHII',...
    'sigmaxHII','VLSR','VLSR_m','VLSR_s','df','dfmin','dfmax')
C = coeffvalues(fitresult);
gauss1 = fitmodel(C(1),0,0,0,0,0,C(7),0,0,C(10),0,0,0,freq);
gauss2 = fitmodel(0,C(2),0,0,0,0,0,C(8),0,0,C(11),0,0,freq);
gauss3 = fitmodel(0,0,C(3),0,0,0,0,C(8),0,0,0,C(12),0,freq);
gauss4 = fitmodel(0,0,0,C(4),0,0,0,0,C(9),0,0,0,C(13),freq);
figure
plot(freq,gauss1,freq,gauss2,freq,gauss3,freq,gauss4)
clear('gauss1','gauss2','gauss3','gauss4')

% Integrated flux.
Cint = confint(fitresult);
dC = Cint(2,:)-C;
[Flux1, dFluxRel1] = GaussT2Flux(C(2),C(11)*1E9,f0*1E9,dC(2),dC(11)*1E9,...
    HIFIBand);
[Flux2, dFluxRel2] = GaussT2Flux(C(3),C(12)*1E9,f0*1E9,dC(3),dC(12)*1E9,...
    HIFIBand);
Flux = Flux1+Flux2;
dFluxRel = sqrt(dFluxRel1^2+dFluxRel2^2 + GetHIFIUncert(HIFIBand)^2);
dFlux = Flux*dFluxRel/100;
disp(['Carina : [CII] Line flux = ' num2str(Flux) ' +- ' num2str(dFlux) ...
    ' (' num2str(ceil(100*dFluxRel)/100) '%) W/m^2/sr']);
clear('C','Cint','dC','dFluxRel1','dFluxRel2')

% Save data to output data file.
Object = 'Carina';
ValueType = 'Flux';
LineFluxes(1+3*(IndLine-1)+1,:) = {Instru,Line,wvl,FWHM,Object,ValueType,Flux,dFluxRel};
clear('Object','ValueType')
save('LineFluxes.mat','LineFluxes')

%% [CII] Line flux : HST 10
% Load data from the FITS file.
path = '../Spectra/';
file = 'HST10_CII_HIFI_WBS_H_USB.fits';
filepath = [path file];
[freqH,TantennaH] = ReadHifiFitsSpectrum(filepath,3);
file = 'HST10_CII_HIFI_WBS_V_USB.fits';
filepath = [path file];
[freqV,TantennaV] = ReadHifiFitsSpectrum(filepath,3);
% Mean spectrum.
[freq, Tantenna] = MeanSpectrum(freqH, TantennaH, freqV, TantennaV);
clear('path','file','filepath','freqH','TantennaH','freqV','TantennaV')

% Fit.
freqlim = 1900.5;
index = freq<freqlim;
freq = freq(index);
Tantenna = Tantenna(index);
gin = [1900.37822580645;1900.43306451613;1900.40241935484;...
    1900.54596774194;1900.44596774194;1900.48951612903];
[fitresult, fitmodel] = gaussfit(freq,Tantenna,3,3,'nointeraction',0,gin);
C = coeffvalues(fitresult);
gaussplot(fitmodel, C, freq)
clear('gin','freqlim','index')

% Integrated flux.
Cint = confint(fitresult);
dC = Cint(2,:)-C;
[Flux, dFluxRel] = GaussT2Flux(C(1),C(9)*1E9,f0*1E9,dC(1),dC(9)*1E9,...
    HIFIBand);
dFluxRel = sqrt(dFluxRel^2 + GetHIFIUncert(HIFIBand)^2);
dFlux = Flux*dFluxRel/100;
disp(['HST 10 : [CII] Line flux = ' num2str(Flux) ' +- ' num2str(dFlux) ...
    ' (' num2str(ceil(100*dFluxRel)/100) '%) W/m^2/sr']);
clear('C','Cint','dC')

% Save data to output data file.
Object = 'HST 10';
ValueType = 'Flux';
LineFluxes(1+3*(IndLine-1)+2,:) = {Instru,Line,wvl,FWHM,Object,ValueType,Flux,dFluxRel};
clear('Object','ValueType')
save('LineFluxes.mat','LineFluxes')

%% [CII] Line flux : 244-440
% Load data from the FITS file.
path = '../Spectra/';
file = 'H244_440_CII_HIFI_WBS_H_USB.fits';
filepath = [path file];
[freqH,TantennaH] = ReadHifiFitsSpectrum(filepath,3);
file = 'H244_440_CII_HIFI_WBS_V_USB.fits';
filepath = [path file];
[freqV,TantennaV] = ReadHifiFitsSpectrum(filepath,3);
% Mean spectrum.
[freq, Tantenna] = MeanSpectrum(freqH, TantennaH, freqV, TantennaV);
clear('path','file','filepath','freqH','TantennaH','freqV','TantennaV')

% Fit.
freqlim = [1900.3 1900.5];
index = freq>freqlim(1) & freq<freqlim(2);
freq = freq(index);
Tantenna = Tantenna(index);
gin = [1900.38951612903;1900.41693548387;1900.40080645161;1900.52661290323;...
    1900.41854838710;1900.51209677419];
[fitresult, fitmodel] = gaussfit(freq,Tantenna,3,3,...
    'nointeraction',0,gin);
C = coeffvalues(fitresult);
gaussplot(fitmodel,C,freq);
clear('gin','freqlim','index')

% Integrated flux.
Cint = confint(fitresult);
dC = Cint(2,:)-C;
[Flux, dFluxRel] = GaussT2Flux(C(1),C(9)*1E9,f0*1E9,dC(1),dC(9)*1E9,...
    HIFIBand);
dFluxRel = sqrt(dFluxRel^2 + GetHIFIUncert(HIFIBand)^2);
dFlux = Flux*dFluxRel/100;
disp(['244-440 : [CII] Line flux = ' num2str(Flux) ' +- ' num2str(dFlux) ...
    ' (' num2str(ceil(100*dFluxRel)/100) '%) W/m^2/sr']);
clear('C','Cint','dC')

% Save data to output data file.
Object = '244-440';
ValueType = 'Flux';
LineFluxes(1+3*(IndLine-1)+3,:) = {Instru,Line,wvl,FWHM,Object,ValueType,Flux,dFluxRel};
clear('Object','ValueType')
save('LineFluxes.mat','LineFluxes')