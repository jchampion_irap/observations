%% Workspace cleaning
clear all
close all
clc

%% Constant parameters
k = 1.3806488E-23; % Boltzmann constant (J/K).
c = 299792458; % Light velocity (m/s).
f0 = 2185.13468; % Line frequency (Ghz).
PACSBand = 'R1'; %  Band of PACS used in the observation.

% Parameters for the output data file.
Instru = 'PACS';
Line = 'CO 19-18';
IndLine = 6;
load('LineFluxes.mat')

% Wavelength and FWHM.
wvl = c*1e6/(f0*1e9); % Wavelength in micrometers.
FWHM = 9.4; 

%% CO 19-18 Line flux : HST 10
% Load data from the FITS file.
path = '../Spectra/';
file = 'HST10_CO_PACSSpec_137um.fits';
filepath = [path file];
data = fitsread(filepath,'binarytable');
lambda = data{1}*1E-6;
int = data{3};
lbounds = [136.7 137.5]*1E-6;
ind = lambda > lbounds(1) & lambda < lbounds(2);
lambda = lambda(ind);
int = int(ind);
freq = c./lambda;
freq_Ghz = freq*1E-9;
% plot(freq_Ghz,int);
% axe = axis;
% dv=-26000;
% f0doppler = f0+dv*f0/c;
% line([f0doppler f0doppler],[axe(3) axe(4)])

% Fit.
gin = [2183.04133064516;2186.82358870968];
[fitresult, fitmodel] = gaussfit(freq_Ghz,int,1,1,...
    'nointeraction',0,gin);
C = coeffvalues(fitresult);
Cint = confint(fitresult);
dC = Cint(2,:)-C;
gaussplot(fitmodel, C, freq_Ghz)
clear('gin')

% Line width used to estimate detection limit for the Carina Proplyd.
std_gauss_signal = C(5);
std_gauss_signal_err = dC(5);

% Integrated flux.
[Flux, dFluxRel] = GaussJy2Flux(C(1),C(5)*1E9,dC(1),dC(5)*1E9);
dFluxRel = sqrt(dFluxRel^2 + GetPACSUncert(PACSBand)^2);
dFlux = Flux*dFluxRel/100;
% disp(['HST 10 : CO 19-18 Line flux < Neb = ' num2str(Flux) ' +- ' num2str(dFlux) ...
%     ' (' num2str(ceil(100*dFluxRel)/100) '%) W/m^2/pixel']);
clear('C','Cint','dC')
pix = (9.4/3600 * pi/180)^2;
FluxPerSr = Flux / pix;
dFluxPerSr = dFlux / pix;
disp(['HST 10 : CO 19-18 Line flux < Neb = ' num2str(FluxPerSr) ' +- ' ...
    num2str(dFluxPerSr) ...
    ' (' num2str(ceil(100*dFluxRel)/100) '%) W/m^2/sr']);
clear('Flux','dFlux','dFluxPerSr')

% Save data to output data file.
Object = 'HST 10';
ValueType = 'NebSupFlux';
LineFluxes(1+3*(IndLine-1)+2,:) = {Instru,Line,wvl,FWHM,Object,ValueType,FluxPerSr,dFluxRel};
clear('Object','ValueType')
save('LineFluxes.mat','LineFluxes')

%% CO 19-18 Line flux : 244-440
% Load data from the FITS file.
path = '../Spectra/';
file = 'H244_440_CO_PACSSpec_137um.fits';
filepath = [path file];
data = fitsread(filepath,'binarytable');
lambda = data{1}*1E-6;
int = data{3};
lbounds = [136.3 137.8]*1E-6;
ind = lambda > lbounds(1) & lambda < lbounds(2);
lambda = lambda(ind);
int = int(ind);
freq = c./lambda;
freq_Ghz = freq*1E-9;
% dv=-25000;
% f0doppler = f0+dv*f0/c;

% Fit.
gin = [2182.64868951613;2186.55493951613];
[fitresult, fitmodel] = gaussfit(freq_Ghz,int,1,1,...
    'nointeraction',0,gin);
C = coeffvalues(fitresult);
Cint = confint(fitresult);
dC = Cint(2,:)-C;
gaussplot(fitmodel, C, freq_Ghz)
clear('gin')

% Line width used to estimate detection limit for the Carina Proplyd.
std_gauss_signal = 0.5*(C(5)+std_gauss_signal); % Mean.
std_gauss_signal_err = sqrt(dC(5)^2+std_gauss_signal_err^2); % Quadratic sum of the uncertainties.

% Integrated flux.
[Flux, dFluxRel] = GaussJy2Flux(C(1),C(5)*1E9,dC(1),dC(5)*1E9);
dFluxRel = sqrt(dFluxRel^2 + GetPACSUncert(PACSBand)^2);
dFlux = Flux*dFluxRel/100;
% disp(['244-440 : CO 19-18 Line flux < Neb = ' num2str(Flux) ' +- ' num2str(dFlux) ...
%     ' (' num2str(ceil(100*dFluxRel)/100) '%) W/m^2/pixel']);
clear('C','Cint','dC')
pix = (9.4/3600 * pi/180)^2;
FluxPerSr = Flux / pix;
dFluxPerSr = dFlux / pix;
disp(['244-440 : CO 19-18 Line flux < Neb = ' num2str(FluxPerSr) ' +- ' ...
    num2str(dFluxPerSr) ...
    ' (' num2str(ceil(100*dFluxRel)/100) '%) W/m^2/sr']);
clear('Flux','dFlux','dFluxPerSr')

% Save data to output data file.
Object = '244-440';
ValueType = 'NebSupFlux';
LineFluxes(1+3*(IndLine-1)+3,:) = {Instru,Line,wvl,FWHM,Object,ValueType,FluxPerSr,dFluxRel};
clear('Object','ValueType')
save('LineFluxes.mat','LineFluxes')

%% CO 19-18 Line flux : Carina
% Load data from the FITS file.
path = '../Spectra/';
file = 'CARINA_CO_PACSSpec_137um.fits';
filepath = [path file];
data = fitsread(filepath,'binarytable');
lambda = data{1}*1E-6;
int = data{3};
lbounds = [136.5 138.2]*1E-6;
ind = lambda > lbounds(1) & lambda < lbounds(2);
lambda = lambda(ind);
int = int(ind);
freq = c./lambda;
freq_Ghz = freq*1E-9;
% plot(freq_Ghz,int);
% axe = axis;
dv=22500;
f0doppler = f0+dv*f0/c;
% line([f0doppler f0doppler],[axe(3) axe(4)])
clear('path','file','filepath','data','lbounds','ind','axe','dv')

% Noise
Noise = NoiseEstimation(freq_Ghz,int,1,1,f0doppler);
[minflux, dminfluxRel] = GaussJy2Flux(2*Noise,std_gauss_signal*1E9,0,...
    std_gauss_signal_err*1E9);
dminfluxRel = sqrt(dminfluxRel^2 + GetPACSUncert(PACSBand)^2);
dminflux = minflux*dminfluxRel/100;

disp(['Carina : CO 19-18 Line peak < ' num2str(2*Noise) ...
    ' W/m^2/sr/Hz (@ 2 times the noise standard deviation).']);
pix = (9.4/3600 * pi/180)^2;
FluxPerSr = minflux / pix;
dFluxPerSr = dminflux / pix;
disp(['Carina : CO 19-18 Line flux < ' num2str(FluxPerSr) ' +- ' num2str(dFluxPerSr) ...
    ' (' num2str(ceil(100*dminfluxRel)/100) '%) W/m^2/sr ' char(10) ...
    '(For an intensity equals to 2 * noise standard deviation and a ' ...
    'line width assessed from the detection on the Orion Proplyds.)']);
clear('dv','f0doppler','pix')

% Save data to output data file.
Object = 'Carina';
ValueType = 'SupFlux';
LineFluxes(1+3*(IndLine-1)+1,:) = {Instru,Line,wvl,FWHM,Object,ValueType,FluxPerSr,dFluxRel};
clear('Object','ValueType')
save('LineFluxes.mat','LineFluxes')