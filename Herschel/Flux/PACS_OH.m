%% Workspace cleaning
clear all
close all
clc

%% Constant parameters
k = 1.3806488E-23; % Boltzmann constant (J/K).
c = 299792458; % Light velocity (m/s).
f0 = 3550; % Line frequency (Ghz).
PACSBand = 'B2B'; %  Band of PACS used in the observation.

% Parameters for the output data file.
Instru = 'PACS';
Line = 'OH';
IndLine = 8;
load('LineFluxes.mat')

% Wavelength and FWHM.
wvl = c*1e6/(f0*1e9); % Wavelength in micrometers.
FWHM = 9.4; 

%% OH Line flux : 244-440
% Load data from the FITS file.
path = '../Spectra/';
dat = fitsread([path 'H244_440_OH_PACSSpec.fits'],'binarytable');
lambda = dat{3}; % Wavelength in micrometers.
int = dat{1}; % Flux in Jy/pixel.
clear('path','dat')

% Select range near the lines.
lbounds = [84.3 84.7];
index = lambda >= lbounds(1) & lambda <= lbounds(2);
int = int(index);
lambda = lambda(index);
clear('lbounds','index')

% Conversion in frequency.
c = c*1E6 ; % Light velocity in micrometers/s.
freq = c./lambda;
freq_Thz = freq*1E-12;

% Fit
gin=[3.54123739919355;3.54544606854839;3.54862147177419;3.55403981854839];
[fitresult, fitmodel] = gaussfit(freq_Thz,int,2,2,'nointeraction',0,gin);
C = coeffvalues(fitresult);
Cint = confint(fitresult);
dC = Cint(2,:)-C;

% Integrated flux.
[Flux1, dFluxRel1] = GaussJy2Flux(C(1),C(7)*1E12,dC(1),dC(7)*1E12);
[Flux2, dFluxRel2] = GaussJy2Flux(C(2),C(8)*1E12,dC(2),dC(8)*1E12);
Flux = Flux1+Flux2;
dFluxRel = sqrt(dFluxRel1^2+dFluxRel2^2);
dFluxRel = sqrt(dFluxRel^2 + GetPACSUncert(PACSBand)^2);
dFlux = Flux*dFluxRel/100;
% disp(['244-440 : OH Line flux < Neb = ' num2str(Flux) ' +- ' num2str(dFlux) ...
%     ' (' num2str(ceil(100*dFluxRel)/100) '%) W/m^2/pixel']);
clear('C','Cint','dC','Flux1','dFluxRel1','Flux2','dFluxRel2')
pix = (9.4/3600 * pi/180)^2;
FluxPerSr = Flux / pix;
dFluxPerSr = dFlux / pix;
disp(['244-440 : OH Line flux = ' num2str(FluxPerSr) ' +- ' ...
    num2str(dFluxPerSr) ...
    ' (' num2str(ceil(100*dFluxRel)/100) '%) W/m^2/sr']);
clear('pix','Flux','dFlux','dFluxPerSr')

% Save data to output data file.
Object = '244-440';
ValueType = 'Flux';
LineFluxes(1+3*(IndLine-1)+3,:) = {Instru,Line,wvl,FWHM,Object,ValueType,FluxPerSr,dFluxRel};
clear('Object','ValueType')
save('LineFluxes.mat','LineFluxes')

% Save data to output data file for the two other objects.
Object = 'Carina';
ValueType = 'Flux';
LineFluxes(1+3*(IndLine-1)+1,:) = {Instru,Line,wvl,FWHM,Object,ValueType,NaN,NaN};
clear('Object','ValueType')
save('LineFluxes.mat','LineFluxes')
Object = 'HST 10';
ValueType = 'Flux';
LineFluxes(1+3*(IndLine-1)+2,:) = {Instru,Line,wvl,FWHM,Object,ValueType,NaN,NaN};
clear('Object','ValueType')
save('LineFluxes.mat','LineFluxes')

%{
%% Better fitted continuum
figure
set(gcf,'units','normalized')
set(gcf,'position',[0 0 1 1])
% Get a larger range in frequency.
lambda = dat{3}; % Wavelength in micrometers.
int = dat{1}; % Flux in Jy/pixel.
% Suppress bad data.
lbounds = [83.915 85.125];
index = lambda >= lbounds(1) & lambda <= lbounds(2);
int = int(index);
lambda = lambda(index);

% Conversion in frequency.
c = 299792458; % Light velocity in meters/s.
c = c*1E6 ; % Light velocity in micrometers/s.
freq = c./lambda;
freq_Thz = freq*1E-12;

% Continuum
cont = int - fitmodel(fitresult.A1,fitresult.A2,0,0,...
    fitresult.m1,fitresult.m2,fitresult.s1,fitresult.s2,freq_Thz);
intcont = mean(cont(:));
stdcont = std(cont(:));
disp(['Carina OH : Cont = ' num2str(intcont) ' +- ' num2str(stdcont) ...
    ' Jy/pixel @ ' num2str(mean(lambda)) ' micrometers']);
pix = (9.4/3600 * pi/180)^2;
intcontPerSr = intcont*1E-26 / pix;
stdcontPerSr = stdcont*1E-26 / pix;
disp(['Carina OH : Cont = ' num2str(intcontPerSr) ' +- ' ...
    num2str(stdcontPerSr) ...
    ' W/m^2/sr/Hz @ ' num2str(mean(lambda)) ' micrometers']);

int_continuum = int - fitmodel(fitresult.A1,fitresult.A2,0,0,...
    fitresult.m1,fitresult.m2,fitresult.s1,fitresult.s2,freq_Thz);
    
plot(freq_Thz,int_continuum,'.-b')
hold on
plot(freq_Thz,int,'--')

coeffpol = coeffvalues(fpol);
res = int_continuum-fitline;
plot(freq_Thz,res,'.-g')
fs = 15;
xlabel('Frequency (THz)','fontsize',fs)
ylabel('Flux (Jy/pixel)','fontsize',fs)
legend('Extracted continuum','Original data','Fitted straight line',...
    'Residuals')
set(gca,'fontsize',fs)
flux = mean(int_continuum);
pix = (9.4*pi/(3600*180))^2;
flux_SI = flux*1E-26/pix;
title(['Mean continuum flux : ' num2str(flux) ' Jy/pixel (' num2str(flux_SI) ...
    ' W/m^2/sr/Hz)'])
%}