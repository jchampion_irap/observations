function [Flux, DeltaFluxRel] = GaussJy2Flux(A,s,dA,ds)

% Conversion Jy to W/m^2/Hz
A_SI = A*1E-26;

% Convert uncertainties from 95% (i.e. 1.96 sigma) to 68.2% (one sigma).
dA = dA/1.96;
ds = ds/1.96;

% Integration.
Flux = sqrt(2*pi)*A_SI*s;
if exist('dA','var') && exist('ds','var')
    DeltaFluxRel = sqrt((dA/A)^2+(ds/s)^2)*100; % Relative uncertainty in %.
end

end