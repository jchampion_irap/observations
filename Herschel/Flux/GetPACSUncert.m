function PACSUncert = GetPACSUncert(PACSBand)

% Absolute flux calibration accuracies. The following table provides the
% RMS error (in percentage) associated with spectral band of PACS
% spectrometer. Column 1 to 4 are respectively values for Band B2A (50-70 
% micrometer), B3A (50-70 micrometer), B2B (70-100 micrometer) and R1 
% (100-220 micrometer). 
% See http://herschel.esac.esa.int/Docs/PACS/html/ch04s10.html#sec-spectro-fluxcal-sub2
PACSUncert = [11 11 12 12];

% Get the error for the band.
switch PACSBand
    case 'B2A'
        PACSUncert = PACSUncert(1);
    case 'B3A'
        PACSUncert = PACSUncert(2);
    case 'B2B'
        PACSUncert = PACSUncert(3);
    case 'R1'
        PACSUncert = PACSUncert(4);
    otherwise
        error('PACSBand have to be ''B2A'', ''B3A'', ''B2B'' or ''R1''.');
end

end