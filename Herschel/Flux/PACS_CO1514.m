%% Workspace cleaning
clear all
close all
clc

%% Constant parameters
k = 1.3806488E-23; % Boltzmann constant (J/K).
c = 299792458; % Light velocity (m/s).
f0 = 1726.60251; % Line frequency (Ghz).
PACSBand = 'R1'; %  Band of PACS used in the observation.

% Parameters for the output data file.
Instru = 'PACS';
Line = 'CO 15-14';
IndLine = 4;
load('LineFluxes.mat')

% Wavelength and FWHM.
wvl = c*1e6/(f0*1e9); % Wavelength in micrometers.
FWHM = 9.4; 

%% CO 15-14 Line flux : Carina
% Load data from the FITS file.
path = '../Spectra/';
file = 'CARINA_CO_PACSSpec_173um.fits';
filepath = [path file];
data = fitsread(filepath,'binarytable');
lambda = data{1}*1E-6;
int = data{3};
lbounds = [172.9 174.3]*1E-6;
ind = lambda > lbounds(1) & lambda < lbounds(2);
lambda = lambda(ind);
int = int(ind);
freq = c./lambda;
freq_Ghz = freq*1E-9;
% plot(freq_Ghz,int);
% axe = axis;
dv=22500;
f0doppler = f0+dv*f0/c;
% line([f0doppler f0doppler],[axe(3) axe(4)])
clear('path','file','filepath','data','lbounds','ind','axe','dv')

% Fit.
gin = [1725.75604838710;1727.11088709677];
[fitresult, fitmodel] = gaussfit(freq_Ghz,int,1,1,...
    'nointeraction',0,gin);
C = coeffvalues(fitresult);
Cint = confint(fitresult);
dC = Cint(2,:)-C;
gaussplot(fitmodel, C, freq_Ghz)
clear('gin')

% Integrated flux.
[Flux, dFluxRel] = GaussJy2Flux(C(1),C(5)*1E9,dC(1),dC(5)*1E9);
dFluxRel = sqrt(dFluxRel^2 + GetPACSUncert(PACSBand)^2);
dFlux = Flux*dFluxRel/100;
% disp(['Carina : CO 15-14 Line flux = ' num2str(Flux) ' +- ' num2str(dFlux) ...
%     ' (' num2str(ceil(100*dFluxRel)/100) '%) W/m^2/pixel']);
clear('Cint','dC')
pix = (9.4/3600 * pi/180)^2; % Conversion in W/m²/sr.
FluxPerSr = Flux / pix;
dFluxPerSr = dFlux / pix;
disp(['Carina : CO 15-14 Line flux = ' num2str(FluxPerSr) ' +- ' ...
    num2str(dFluxPerSr) ...
    ' (' num2str(ceil(100*dFluxRel)/100) '%) W/m^2/sr']);
clear('pix','Flux','dFlux','dFluxPerSr')

% Noise
int_noise = int - fitmodel(C(1),C(2),C(3),C(4),C(5),freq_Ghz);
Noise = NoiseEstimation(freq_Ghz,int_noise,1,1,f0doppler);
hold on
plot(freq_Ghz,int-fitmodel(0,C(2),C(3),0,0,freq_Ghz),'b--')
disp(['Detection @ ' num2str(fitresult.A1/Noise) ' times the noise standard deviation.']);
clear('C')

% Save data to output data file.
Object = 'Carina';
ValueType = 'Flux';
LineFluxes(1+3*(IndLine-1)+1,:) = {Instru,Line,wvl,FWHM,Object,ValueType,FluxPerSr,dFluxRel};
clear('Object','ValueType')
save('LineFluxes.mat','LineFluxes')

%% CO 15-14 Line flux : HST 10
% Load data from the FITS file.
path = '../Spectra/';
file = 'HST10_CO_PACSSpec_173um.fits';
filepath = [path file];
data = fitsread(filepath,'binarytable');
lambda = data{1}*1E-6;
int = data{3};
lbounds = [173 174.5]*1E-6;
ind = lambda > lbounds(1) & lambda < lbounds(2);
lambda = lambda(ind);
int = int(ind);
freq = c./lambda;
freq_Ghz = freq*1E-9;
% dv=-26000;
% f0doppler = f0+dv*f0/c;
clear('path','file','filepath','data','lbounds','ind')

% Fit.
gin = [1725.00806451613;1728.45967741935];
[fitresult, fitmodel] = gaussfit(freq_Ghz,int,1,1,...
    'nointeraction',0,gin);
C = coeffvalues(fitresult);
Cint = confint(fitresult);
dC = Cint(2,:)-C;
gaussplot(fitmodel, C, freq_Ghz)
clear('gin')

% Integrated flux.
[Flux, dFluxRel] = GaussJy2Flux(C(1),C(5)*1E9,dC(1),dC(5)*1E9);
dFluxRel = sqrt(dFluxRel^2 + GetPACSUncert(PACSBand)^2);
dFlux = Flux*dFluxRel/100;
% disp(['HST 10 : CO 15-14 Line flux < Neb = ' num2str(Flux) ' +- ' num2str(dFlux) ...
%     ' (' num2str(ceil(100*dFluxRel)/100) '%) W/m^2/pixel']);
clear('C','Cint','dC')
pix = (9.4/3600 * pi/180)^2;
FluxPerSr = Flux / pix;
dFluxPerSr = dFlux / pix;
disp(['HST 10 : CO 15-14 Line flux < Neb = ' num2str(FluxPerSr) ' +- ' ...
    num2str(dFluxPerSr) ...
    ' (' num2str(ceil(100*dFluxRel)/100) '%) W/m^2/sr']);
clear('pix','Flux','dFlux','dFluxPerSr')

% Save data to output data file.
Object = 'HST 10';
ValueType = 'NebSupFlux';
LineFluxes(1+3*(IndLine-1)+2,:) = {Instru,Line,wvl,FWHM,Object,ValueType,FluxPerSr,dFluxRel};
clear('Object','ValueType')
save('LineFluxes.mat','LineFluxes')

%% CO 15-14 Line flux : 244-440
% Load data from the FITS file.
path = '../Spectra/';
file = 'H244_440_CO_PACSSpec_173um.fits';
filepath = [path file];
data = fitsread(filepath,'binarytable');
lambda = data{1}*1E-6;
int = data{3};
lbounds = [172.5 174.5]*1E-6;
ind = lambda > lbounds(1) & lambda < lbounds(2);
lambda = lambda(ind);
int = int(ind);
freq = c./lambda;
freq_Ghz = freq*1E-9;
% dv=-25000;
% f0doppler = f0+dv*f0/c;
clear('path','file','filepath','data','lbounds','ind')

% Fit.
gin = [1724.98588709677;1728.37298387097];
[fitresult, fitmodel] = gaussfit(freq_Ghz,int,1,1,...
    'nointeraction',0,gin);
C = coeffvalues(fitresult);
Cint = confint(fitresult);
dC = Cint(2,:)-C;
gaussplot(fitmodel, C, freq_Ghz)
clear('gin')

% Integrated flux.
[Flux, dFluxRel] = GaussJy2Flux(C(1),C(5)*1E9,dC(1),dC(5)*1E9);
dFluxRel = sqrt(dFluxRel^2 + GetPACSUncert(PACSBand)^2);
dFlux = Flux*dFluxRel/100;
% disp(['244-440 : CO 15-14 Line flux < Neb = ' num2str(Flux) ' +- ' num2str(dFlux) ...
%     ' (' num2str(ceil(100*dFluxRel)/100) '%) W/m^2/pixel']);
clear('C','Cint','dC')
pix = (9.4/3600 * pi/180)^2;
FluxPerSr = Flux / pix;
dFluxPerSr = dFlux / pix;
disp(['244-440 : CO 15-14 Line flux < Neb = ' num2str(FluxPerSr) ' +- ' ...
    num2str(dFluxPerSr) ...
    ' (' num2str(ceil(100*dFluxRel)/100) '%) W/m^2/sr']);
clear('pix','Flux','dFlux','dFluxPerSr')

% Save data to output data file.
Object = '244-440';
ValueType = 'NebSupFlux';
LineFluxes(1+3*(IndLine-1)+3,:) = {Instru,Line,wvl,FWHM,Object,ValueType,FluxPerSr,dFluxRel};
clear('Object','ValueType')
save('LineFluxes.mat','LineFluxes')