function LineFlux2Tex()

% Get data
if exist('LineFluxes.mat','file')
    LF = GetLineFluxes(0);
else
    LF = GetLineFluxes(1);
end
LF(1,:) = [];
obj = {'Carina','HST 10','244-440'};
Nobj = length(obj);
Nlines = size(LF,1)/Nobj;

% Opening text file
fid = fopen('table_linefluxes.tex','w');

% Writing in file
fprintf(fid,'%s\n','\small');
fprintf(fid,'%s\n','\begin{tabular}{|l|c|c|c|c|}');
fprintf(fid,'%s\n','\hline');
fprintf(fid,'%s\n',['& & \textbf{Carina proplyd} ' ...
    '& \textbf{HST 10}' ...
    '& \textbf{244-440} \\']);
fprintf(fid,'%s\n','\hline');

indobj = zeros(1,Nobj);
for it = 1:Nobj
    indobj(it) = find(strcmp(LF(:,5),obj(it)),1,'first');
end

prec_val = '%7.2e';
prec_uncpc = '%2.0f';
[~, indinstru] = unique(LF(1:Nobj:end,1));
linstru = diff([indinstru(:);Nlines+1]);

for l = 1:Nlines
    indfirstinstru = find((indinstru == l)==1, 1,'first');
    if indfirstinstru > 1
        fprintf(fid,'%s\n','\hline');
    end
    if ~isempty(indfirstinstru)
        txtline = ['\multirow{' num2str(linstru(indfirstinstru)) ...
            '}*{' LF{(l-1)*Nobj+1,1} ...
            '} & ' LF{(l-1)*3+1,2} ' & ' ];
    else
        txtline = ['& ' LF{(l-1)*3+1,2} ' & ' ];
    end
    for itobj = 1:Nobj
        if strcmp(LF{(l-1)*Nobj+indobj(itobj),6},'Flux')
            prestr = '~~~';
            poststr = '$^{~~}$';
        elseif strcmp(LF{(l-1)*Nobj+indobj(itobj),6},'SupFlux')
            prestr = '< ';
            poststr = '$^{*~}$';
        elseif strcmp(LF{(l-1)*Nobj+indobj(itobj),6},'NebSupFlux')
            prestr = '< ';
            poststr = '$^{**}$';
        else
            error('Error in table -> Flux/SupFlux/NebSupFlux');
        end
        if ~isnan(LF{(l-1)*Nobj+indobj(itobj),8})
            txtline = [txtline prestr ...
                num2str(LF{(l-1)*Nobj+indobj(itobj),7},prec_val) ...
                poststr ' (' ...
                num2str(ceil(LF{(l-1)*Nobj+indobj(itobj),8}),prec_uncpc) ...
                '\%) & ']; %#ok<*AGROW>
        else
            txtline = [txtline 'X & '];
        end
    end
    txtline(end-1:end) = '\\';
    fprintf(fid,'%s\n',txtline);
end

fprintf(fid,'%s\n','\hline');
fprintf(fid,'%s\n','\end{tabular}');
fprintf(fid,'%s\n','\normalsize');

% Closin text file
fclose(fid);

end