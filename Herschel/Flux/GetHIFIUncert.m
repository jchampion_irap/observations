function HIFIUncert = GetHIFIUncert(HIFIBand)

% Overall error budget. The following table provides the percentage flux
% error associated with each component of the error budget. Column 2 to 5
% are respectively values for Band 1/2, 3/4, 5 and 6/7.
% See http://herschel.esac.esa.int/Docs/HIFI/html/ch05s07.html
Table = {...
    'Sideband ratio' 4 6 6 8 ;...
    'Hot load coupling' 1 1 1 1 ;...
    'Cold load coupling' 1 1 1 1 ;...
    'Hot load temperature' 1 1 1 1 ;...
    'Cold load temperature' 1 1 1 1 ;...
    'Planetary model error' 3 3 3 3 ;...
    'Beam Efficiency' 5 5 10 5 ;...
    'Pointing' 1 2 2 4 ;...
    'Opt. standing waves' 4 4 3 3};
TableNum = cell2mat(Table(:,2:5));

% Combinaison of systematic errors with a quadratically sum.
HIFIUncert = sqrt(sum(TableNum.^2,1));
switch HIFIBand
    case {1,2}
        HIFIUncert = HIFIUncert(1);
    case {3,4}
        HIFIUncert = HIFIUncert(2);
    case 5
        HIFIUncert = HIFIUncert(3);
    case {6,7}
        HIFIUncert = HIFIUncert(4);
end

clear('Table','TableNum')

end