function [Flux, DeltaFluxRel] = GaussT2Flux(A,s,f0,dA,ds,HIFIBand)

% Constant.
k = 1.3806488E-23;
c = 299792458;

% % Correct temperature with the main beam efficiency.
% % See http://herschel.esac.esa.int/Docs/HIFI/html/ch05s05.html.
% eta_mb = [0.76 0.75 0.75 0.74 0.64 0.72 0.69];
% A = A/eta_mb(HIFIBand);
% Take the main beam efficiency in uncertainties and take a mean value
% between the case uncorrected and the corrected case.
eta_mb = [0.76 0.75 0.75 0.74 0.64 0.72 0.69];
A = mean([A A/eta_mb(HIFIBand)]);
DeltaFluxRel = (1-eta_mb(HIFIBand))/2;

% Conversion K to W/m^2/sr.
lambda = c/f0;
A_SI = A*2*k/lambda^2;

% Convert uncertainties from 95% (i.e. 1.96 sigma) to 68.2% (one sigma).
dA = dA/1.96;
ds = ds/1.96;

% Integration.
Flux = sqrt(2*pi)*A_SI*s;
if exist('dA','var') && exist('ds','var')
    DeltaFluxRel = sqrt(DeltaFluxRel^2+(dA/A)^2+(ds/s)^2)*100; % Relative uncertainty in %.
end

end