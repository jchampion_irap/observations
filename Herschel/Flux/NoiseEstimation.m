function Noise = NoiseEstimation(x,y,polyfitdegree,figdisplay, xc)

if figdisplay == 1
    figure;
    plot(x,y)
end
[P,S] = polyfit(x,y,polyfitdegree);
y = y - polyval(P,x,S);
Noise = std(y);
if figdisplay == 1
    hold on
    plot(x,y,'r')
    figure
    hist(y,fix(length(y)/5));
    figure;
    hold on;
    plot(x,y,'b','linewidth',2)
    axe = axis;
    axe(3:4) = [-4*Noise 4*Noise];
    axis(axe);
    if exist('xc','var')
        line([xc xc],[axe(3) axe(4)],'color','k','linewidth',2)
    end
    p1 = patch([axe(1) axe(1) axe(2) axe(2)],[-Noise +Noise +Noise -Noise],'r');
    alpha(p1,0.30);
    p2p = patch([axe(1) axe(1) axe(2) axe(2)],[+Noise +2*Noise +2*Noise +Noise],'r');
    alpha(p2p,0.20);
    p2m = patch([axe(1) axe(1) axe(2) axe(2)],-[+Noise +2*Noise +2*Noise +Noise],'r');
    alpha(p2m,0.20);
    p3p = patch([axe(1) axe(1) axe(2) axe(2)],[+2*Noise +3*Noise +3*Noise +2*Noise],'r');
    alpha(p3p,0.10);
    p3m = patch([axe(1) axe(1) axe(2) axe(2)],-[+2*Noise +3*Noise +3*Noise +2*Noise],'r');
    alpha(p3m,0.10);
    axis([axe(1) axe(2) -4*Noise 4*Noise]);
end

end