function LineFluxes = GetLineFluxes(update)

if update == 1
    % Create data file with columns as follow :
    % Instrument | Line | Object | Type of value | Value | Uncertainty
    nlinefluxes = 8;
    nobjects = 3;
    LineFluxes = cell(nlinefluxes*nobjects+1,8); 
    LineFluxes(1,:) = {'Instrument','Line','Wavelength (um)','FWHM (")','Object','Type of Value','Value (W/m²/sr)','Uncertainty (%)'};
    save('LineFluxes.mat','LineFluxes')
    
    % HIFI Line fluxes
    HIFI_CII;
    HIFI_CO76;
    HIFI_CO109;
    
    % PACS Line fluxes
    PACS_CO1514;
    PACS_CO1716;
    PACS_CO1918;
    PACS_OI;
    PACS_OH;
else
    load('LineFluxes.mat')
end

end