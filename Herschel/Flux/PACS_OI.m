%% Workspace cleaning
clear all
close all
clc

%% Constant parameters
k = 1.3806488E-23; % Boltzmann constant (J/K).
c = 299792458; % Light velocity (m/s).
f0 = 4744.777; % Line frequency (Ghz).
PACSBand = 'B3A'; %  Band of PACS used in the observation.

% Parameters for the output data file.
Instru = 'PACS';
Line = '[OI]';
IndLine = 7;
load('LineFluxes.mat')

% Wavelength and FWHM.
wvl = c*1e6/(f0*1e9); % Wavelength in micrometers.
FWHM = 9.4; 

%% [OI] Line flux : Carina
% Load data from the FITS file.
path = '../Spectra/';
file = 'CARINA_OI_PACSSpec.fits';
filepath = [path file];
data = fitsread(filepath,'binarytable');
lambda = data{1}*1E-6;
int = data{3};
lbounds = [63.1 63.3]*1E-6;
ind = lambda > lbounds(1) & lambda < lbounds(2);
lambda = lambda(ind);
int = int(ind);
freq = c./lambda;
freq_Ghz = freq*1E-9;
clear('path','file','filepath','data','lbounds','ind','axe','dv')

% Fit.
gin = [4743.92741935484;4746.86290322581];
[fitresult, fitmodel] = gaussfit(freq_Ghz,int,1,1,...
    'nointeraction',0,gin);
C = coeffvalues(fitresult);
Cint = confint(fitresult);
dC = Cint(2,:)-C;
gaussplot(fitmodel, C, freq_Ghz)
clear('gin')

% Integrated flux.
[Flux, dFluxRel] = GaussJy2Flux(C(1),C(5)*1E9,dC(1),dC(5)*1E9);
dFluxRel = sqrt(dFluxRel^2 + GetPACSUncert(PACSBand)^2);
dFlux = Flux*dFluxRel/100;
% disp(['Carina : [OI] Line flux = ' num2str(Flux) ' +- ' num2str(dFlux) ...
%     ' (' num2str(ceil(100*dFluxRel)/100) '%) W/m^2/pixel']);
clear('C','Cint','dC')
pix = (9.4/3600 * pi/180)^2; % Conversion in W/m²/sr.
FluxPerSr = Flux / pix;
dFluxPerSr = dFlux / pix;
disp(['Carina : [OI] Line flux = ' num2str(FluxPerSr) ' +- ' ...
    num2str(dFluxPerSr) ...
    ' (' num2str(ceil(100*dFluxRel)/100) '%) W/m^2/sr']);
clear('pix','Flux','dFlux','dFluxPerSr')

% Save data to output data file.
Object = 'Carina';
ValueType = 'Flux';
LineFluxes(1+3*(IndLine-1)+1,:) = {Instru,Line,wvl,FWHM,Object,ValueType,FluxPerSr,dFluxRel};
clear('Object','ValueType')
save('LineFluxes.mat','LineFluxes')

%% [OI] Line flux : HST 10
% Load data from the FITS file.
path = '../Spectra/';
file = 'HST10_OI_PACSSpec.fits';
filepath = [path file];
data = fitsread(filepath,'binarytable');
lambda = data{1}*1E-6;
int = data{3};
freq = c./lambda;
freq_Ghz = freq*1E-9;
clear('path','file','filepath','data')

% Fit.
gin = [4743.92741935484;4746.86290322581;4743.92741935484;4746.86290322581];
[fitresult, fitmodel] = gaussfit(freq_Ghz,int,2,2,...
    'nointeraction',0,gin);
C = coeffvalues(fitresult);
Cint = confint(fitresult);
dC = Cint(2,:)-C;
gaussplot(fitmodel, C, freq_Ghz)
clear('gin')

% Integrated flux.
[Flux1, dFluxRel1] = GaussJy2Flux(C(1),C(7)*1E9,dC(1),dC(7)*1E9);
[Flux2, dFluxRel2] = GaussJy2Flux(C(2),C(8)*1E9,dC(2),dC(8)*1E9);
Flux = Flux1 + Flux2;
dFluxRel = sqrt(dFluxRel1^2 + dFluxRel2^2);
dFluxRel = sqrt(dFluxRel^2 + GetPACSUncert(PACSBand)^2);
dFlux = Flux*dFluxRel/100;
% disp(['HST 10 : [OI] Line flux < Neb = ' num2str(Flux) ' +- ' num2str(dFlux) ...
%     ' (' num2str(ceil(100*dFluxRel)/100) '%) W/m^2/pixel']);
clear('C','Cint','dC','Flux1','Flux2','dFluxRel1','dFluxRel2')
pix = (9.4/3600 * pi/180)^2;
FluxPerSr = Flux / pix;
dFluxPerSr = dFlux / pix;
disp(['HST 10 : [OI] Line flux < Neb = ' num2str(FluxPerSr) ' +- ' ...
    num2str(dFluxPerSr) ...
    ' (' num2str(ceil(100*dFluxRel)/100) '%) W/m^2/sr']);
clear('pix','Flux','dFlux','dFluxPerSr')

% Save data to output data file.
Object = 'HST 10';
ValueType = 'NebSupFlux';
LineFluxes(1+3*(IndLine-1)+2,:) = {Instru,Line,wvl,FWHM,Object,ValueType,FluxPerSr,dFluxRel};
clear('Object','ValueType')
save('LineFluxes.mat','LineFluxes')

%% [OI] Line flux : 244-440
% Load data from the FITS file.
path = '../Spectra/';
file = 'H244_440_OI_PACSSpec.fits';
filepath = [path file];
data = fitsread(filepath,'binarytable');
lambda = data{1}*1E-6;
int = data{3};
freq = c./lambda;
freq_Ghz = freq*1E-9;
clear('path','file','filepath','data')

% Fit.
gin = [4742;4746];
[fitresult, fitmodel] = gaussfit(freq_Ghz,int,1,1,...
    'nointeraction',0,gin);
C = coeffvalues(fitresult);
Cint = confint(fitresult);
dC = Cint(2,:)-C;
gaussplot(fitmodel, C, freq_Ghz)
clear('gin')

% Integrated flux.
[Flux, dFluxRel] = GaussJy2Flux(C(1),C(5)*1E9,dC(1),dC(5)*1E9);
dFluxRel = sqrt(dFluxRel^2 + GetPACSUncert(PACSBand)^2);
dFlux = Flux*dFluxRel/100;
% disp(['244-440 : [OI] Line flux < Neb = ' num2str(Flux) ' +- ' num2str(dFlux) ...
%     ' (' num2str(ceil(100*dFluxRel)/100) '%) W/m^2/pixel']);
clear('C','Cint','dC')
pix = (9.4/3600 * pi/180)^2;
FluxPerSr = Flux / pix;
dFluxPerSr = dFlux / pix;
disp(['244-440 : [OI] Line flux < Neb = ' num2str(FluxPerSr) ' +- ' ...
    num2str(dFluxPerSr) ...
    ' (' num2str(ceil(100*dFluxRel)/100) '%) W/m^2/sr']);
clear('pix','Flux','dFlux','dFluxPerSr')

% Save data to output data file.
Object = '244-440';
ValueType = 'NebSupFlux';
LineFluxes(1+3*(IndLine-1)+3,:) = {Instru,Line,wvl,FWHM,Object,ValueType,FluxPerSr,dFluxRel};
clear('Object','ValueType')
save('LineFluxes.mat','LineFluxes')

%{
%% OI 244-440 Continuum
% Get data from the FITS file.
path = '../Spectra/';
file = 'H244_440_OI_PACSCube.fits';
filepath = [path file];
cube = fitsread(filepath,'image');
l = fitsread(filepath,'binarytable',4);
l = l{1}*1e-6;
freq = c./l;
freq_Ghz = freq*1E-9;
gin = [4742;4746];

% Get continuum in Jy/pixel.
[AbsCentralCont, dAbsCentralContRel] = CubeContinuum(freq_Ghz,cube,gin);
dAbsCentralCont = AbsCentralCont * dAbsCentralContRel/100;

disp(['244-440 : Cont = ' num2str(AbsCentralCont) ' +- ' ...
    num2str(dAbsCentralCont) ' (' num2str(ceil(dAbsCentralContRel))  ...
    '%) Jy/pixel @ ' num2str(mean(l)*1E6) ' micrometers']);
pix = (9.4/3600 * pi/180)^2;
intcontPerSr = AbsCentralCont*1E-6 / pix; %MJy/sr
stdcontPerSr = dAbsCentralCont*1E-6 / pix;
disp(['244-440 : Cont = ' num2str(intcontPerSr) ' +- ' ...
    num2str(stdcontPerSr) ' (' num2str(ceil(dAbsCentralContRel)) ...
    '%) MJy/sr @ ' num2str(mean(l)*1E6) ' micrometers']);
%}