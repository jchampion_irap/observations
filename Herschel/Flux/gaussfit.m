function [fitresult, fitmodel] = gaussfit(X,Y,ngauss,npos,varargin)
%GAUSSFIT  Fit data with a model composed of a straight line and gaussians
%   functions, using a non linear least squares method with the
%   trust-regions algorithm.
%
%   FITRESULT = GAUSSFIT(X, Y, NGAUSS, NPOS) creates a fit object,
%   FITRESULT, that encapsulates the result of fitting the model containing
%   NGAUSS gaussian functions, of which NPOS are positives (NPOS is set to
%   NGAUSS by default if not given as input argument), to the data X and Y.
%   As a rule, gaussian functions are indexed from 1 to NGAUSS, starting
%   with the positive ones. By calling the function, a plot of the data is
%   realised and you have to click 2 times per gaussian function, in the
%   order of the indexes, approximately at the start and the end of the
%   gaussian's wings to give a first rough estimation of its position and
%   its standard deviation.
%
%   [FITRESULT, FITMODEL] = GAUSSFIT(X, Y, NGAUSS, NPOS) also returns the
%   model as the fittype FITMODEL.
%
%   GAUSSFIT(X, Y, NGAUSS, NPOS, VARARGIN) allow you to specified some
%   constraints about the gaussian functions by the mean of optional
%   arguments in VARARGIN. Each constraint have to be given as a set of
%   three parameters : the name, the gaussian function index and the value.
%   For example GAUSSFIT(X, Y, 3, 2, 'STDLOW', 2, 0.1) will set the minimum
%   value for the standard deviation of the gaussian function number 2 (the
%   last to be positive) to 0.1. You can constraint the bounds of the
%   intensity, the position and the standard deviation (or variance) as
%   well as their initial values used in the fit to insure a better
%   convergence. Possible constraints are thus :
%       - 'INTLOW' : Lower limit of the intensity.
%       - 'INTUP' : Upper limit of the intensity.
%       - 'INTSTART' : Starting value of the intensity.
%       - 'MEANLOW' : Lower limit of the mean, i.e. the position.
%       - 'MEANUP' : Upper limit of the mean, i.e. the position.
%       - 'MEANSTART' : Starting value of the mean, i.e. the position.
%       - 'STDLOW' : Lower limit of the standard deviation.
%       - 'STDUP' : Upper limit of the standard deviation.
%       - 'STDSTART' : Starting value of the standard deviation.
%       - 'VARLOW' : Lower limit of the standard deviation, express in
%           variance.
%       - 'VARUP' : Upper limit of the standard deviation, express in
%           variance.
%       - 'VARSTART' : Starting value of the standard deviation, express un
%           variance.
%   You can also forced 2 or more gaussian functions to share the same
%   mean value, i.e. the same position, using the following optional input
%   arguments in VARARGIN, where INDEX_SUP > INDEX_INF are indexes of the 2
%   gaussian functions sharing their position : 'SAMEMEAN', INDEX_SUP,
%   INDEX_INF.
%   You can avoid user interaction by calling the function with the
%   optional argument NOINTERACTION, e.g. 'NOINTERACTION',0,BOUNDS. BOUNDS
%   is the matrix replacing the result of the ginput function. therefore,
%   BOUNDS must have the dimension (2*NGAUSS)*(1). 0 is only used here
%   because we don't have any gaussian function index to give, please don't
%   try to use anything else to avoid error.
%
%   See also FIT, FITTYPE, FITOPTIONS, CFIT, SFIT.
%
%   =====================================
%   Made by: Jason Champion
%   Date of the last revision: 2014/03/24
%   Last revision: 2.1
%   =====================================
%
%   HISTORY
%   =======
%   * Revision 2.1 (2014/03/24) :
%       - Add the possibility to avoid user interaction by given similar
%         data as the ginput output. 
%   * Revision 2.O (2014/03/21) :
%       - Fit the standard deviation rather than the variance.
%       - Add the possibility to forced 2 or more gaussian functions to
%         share the same position thanks to the optional argument
%         'SAMEMEAN'.
%   * Revision 1.1 (2014/03/20) : 
%       - Add constraint possibilities.
%       - Creation of the help section.
%       - Add the checking of input arguments.
%       - Add comments.
%       - Add instructions for the user, specially for the ginput command.
%   * Revision 1.O (2014/03/19) :
%       - Make a MATLAB function to fit data with several gaussian
%           functions.
%
%   TO DO
%   =====
%   * Add examples.
%


%% Checking input arguments.
% Check number of input arguments.
if nargin < 3
    error('At least, 3 input arguments are expected')
end
% Check X and Y.
if size(X)~=size(Y)
    error('X and Y data must have the same size.')
end
if ~isvector(X) || ~isvector(Y)
    error('X and Y have to be vectors.')
end
X = X(:);
Y = Y(:);
% Avoid NaN Values.
indNaN = find(isnan(Y));
X(indNaN) = [];
Y(indNaN) = [];
% Check NGAUSS and eventually NPOS.
if ~isscalar(ngauss)
    error('NGAUSS have to be a scalar.')
end
if ~exist('npos','var')
    npos = ngauss;
else
    if ~isscalar(npos)
        error('NPOS have to be a scalar.')
    end
end
if npos > ngauss
    error('NPOS have to be inferior or equal to NGAUSS.')
end
% Check constraints in VARARGIN.
if length(varargin)/3~=fix(length(varargin)/3)
    error('Each constraint have to be given by means of three parameters.')
end
for i=1:3:length(varargin)
    if ~ischar(varargin{i})
        error('First argument of a constraint have to be a string.');
    end
end
for i=2:3:length(varargin)
    if varargin{i}~=fix(varargin{i}) || varargin{i}>ngauss
        error(['Second argument of a constraint have to be an integer' ...
            ' inferior or equal to NGAUSS = ' num2str(ngauss) '.'])
    end
end

%% Creating the model: a straight line and NGAUSS gaussian functions.
% Start with a straight line.
fitmodel = 'a0+a1*x';
% Add NGAUSS gaussian functions Ai exp( -(x-mi)^2 / (2*vi) ).
if ~isempty(find(strcmpi(varargin,'samemean')==1, 1))
    indexes = find(strcmpi(varargin,'samemean')==1);
    samemean = cell2mat(varargin(indexes+1));
    samemeanas = cell2mat(varargin(indexes+2));
    nsamemean = length(indexes);
else
    samemean = [];
    nsamemean = 0;
end
for i=1:ngauss
    if isempty(find(samemean==i, 1))
        fitmodel = [fitmodel ' + A' num2str(i) '*exp(-((x-m' num2str(i) ...
            ').^2)./(2*s' num2str(i) '^2))' ]; %#ok<AGROW>
    else
        j=samemeanas(samemean==i);
        fitmodel = [fitmodel ' + A' num2str(i) '*exp(-((x-m' num2str(j) ...
            ').^2)./(2*s' num2str(i) '^2))' ]; %#ok<AGROW>
    end
end
% Convert the string model into a fittype.
fitmodel = fittype(fitmodel);
% Extract the options to modify them.
opt = fitoptions(fitmodel);

%% Initial roughly estimation of gaussian functions.
figure
set(gcf,'units','normalized')
set(gcf,'position',[0 0 1 1])
plot(X,Y,'.-b')
fs = 13;
% Interaction with user.
if ~isempty(find(strcmpi(varargin,'nointeraction')==1, 1))
    ind = find(strcmpi(varargin,'nointeraction')==1,1);
    gin = varargin{ind+2};
else
    if npos~=ngauss
        instructions = title(['Click approximately on the wings of each ',...
            'of the ' num2str(ngauss) ' gaussian functions, starting from ',...
            'the ' num2str(npos) ' positive ones and from left to right.'],...
            'color','r','fontsize',fs,'fontweight','bold');
    else
        instructions = title(['Click approximately on the wings of each ',...
            'of the ' num2str(ngauss) ' gaussian functions, starting from ',...
            'left to right.'],...
            'color','r','fontsize',fs,'fontweight','bold');
    end
    gin = ginput(ngauss*2);
    delete(instructions);
end
instructions = title('Good. Fit in progress...',...
    'color','r','fontsize',fs,'fontweight','bold');

%% Changing options with default values.
% Initialisation of vector that contain bounds and starting positions.
opt.Lower = zeros(1,3*ngauss+2-nsamemean);
opt.Upper = zeros(1,3*ngauss+2-nsamemean);
opt.StartPoint = zeros(1,3*ngauss+2-nsamemean);
% Intensity in [0 Inf] for positive gaussian functions and [-Inf 0] for
% negative ones. Starting value is arbitrary the half range in Y.
for i=1:npos
    opt.Lower(i) = 0;
    opt.Upper(i) = Inf;
    opt.StartPoint(i) = (max(Y(:))-min(Y(:)))/2;
end
for i=npos+1:ngauss
    opt.Lower(i) = -Inf;
    opt.Upper(i) = 0;
    opt.StartPoint(i) = -(max(Y(:))-min(Y(:)))/2;
end
% Y-intercept and slope coefficient in [-Inf Inf], starting at 0.
opt.Lower(ngauss+1:ngauss+2) = [-Inf -Inf];
opt.Upper(ngauss+1:ngauss+2) = [Inf Inf];
opt.StartPoint(ngauss+1:ngauss+2) = [0 0];
% Position and width of gaussian functions.
iswitch = 0;
for i=1:ngauss
    % Position of the gaussian functions in [min(X) max(X)] starting at
    % mean X values given by the interacting user.
    if ~isempty(find(samemean==i, 1))
        iswitch = iswitch + 1;
    else
        opt.Lower(ngauss+2+i-iswitch) = min(X(:));
        opt.Upper(ngauss+2+i-iswitch) = max(X(:));
        opt.StartPoint(ngauss+2+i-iswitch) = ...
            mean([gin(2*(i-1)+1,1) gin(2*(i-1)+2,1)]);
    end
    % Standard deviation of the gaussian functions in [0 Inf] starting at
    % the value calculate from the width of the gaussians' wings given by
    % the interacting user (We estimate that user give approximately the
    % bounds that define a width of 4 sigma, i.e. 4 times the standard
    % deviation.
    opt.Lower(2*ngauss+2+i-nsamemean) = 0;
    opt.Upper(2*ngauss+2+i-nsamemean) = Inf;
    opt.StartPoint(2*ngauss+2+i-nsamemean) = ...
        (gin(2*(i-1)+2,1)-gin(2*(i-1)+1,1))/4;
end

% Algorithm parameters.
opt.Algorithm = 'Trust-Region';
opt.Robust='On';
opt.MaxIter = 1000;
opt.MaxFunEvals = 1000;

%% Changing options with optional constraints. 
for i=1:3:length(varargin)
    % Get the set of 3 parameters.
    condstr = varargin{i};
    indexgauss = varargin{i+1};
    iswitch = length(find(samemean<indexgauss));
    cond = varargin{i+2};
    % Apply the new constraint.
    switch lower(condstr)
        case 'intlow'
            opt.Lower(indexgauss) = cond;
        case 'intup'
            opt.Upper(indexgauss) = cond;    
        case 'intstart'
            opt.StartPoint(indexgauss) = cond;
        case 'meanlow'
            if ~isempty(find(samemean==indexgauss, 1))
                warning(['Constraints about the mean value of the ' ...
                    'gaussian function number ' num2str(indexgauss) ...
                    ' is not taken into account because you set its ' ...
                    'position to be the same as the gaussian function '...
                    'number ' ...
                    num2str(samemeanas(samemean==indexgauss)) '. ',...
                    'Please check the constraints on the latter one.'])
            else
                opt.Lower(2+ngauss+indexgauss-iswitch) = cond;
            end
        case 'meanup'
            if ~isempty(find(samemean==indexgauss, 1))
                warning(['Constraints about the mean value of the ' ...
                    'gaussian function number ' num2str(indexgauss) ...
                    ' is not taken into account because you set its ' ...
                    'position to be the same as the gaussian function '...
                    'number ' ...
                    num2str(samemeanas(samemean==indexgauss)) '. ',...
                    'Please check the constraints on the latter one.'])
            else
                opt.Upper(2+ngauss+indexgauss-iswitch) = cond;
            end
        case 'meanstart'
            if ~isempty(find(samemean==indexgauss, 1))
                warning(['Constraints about the mean value of the ' ...
                    'gaussian function number ' num2str(indexgauss) ...
                    ' is not taken into account because you set its ' ...
                    'position to be the same as the gaussian function '...
                    'number ' ...
                    num2str(samemeanas(samemean==indexgauss)) '. ',...
                    'Please check the constraints on the latter one.'])
            else
                opt.StartPoint(2+ngauss+indexgauss-iswitch) = cond;
            end
        case 'stdlow'
            opt.Lower(2+2*ngauss+indexgauss-nsamemean) = cond;
        case 'stdup'
            opt.Upper(2+2*ngauss+indexgauss-nsamemean) = cond;
        case 'stdstart'
            opt.StartPoint(2+2*ngauss+indexgauss-nsamemean) = cond;
        case 'varlow'
            opt.Lower(2+2*ngauss+indexgauss-nsamemean) = sqrt(cond);
        case 'varup'
            opt.Upper(2+2*ngauss+indexgauss-nsamemean) = sqrt(cond);
        case 'varstart'
            opt.StartPoint(2+2*ngauss+indexgauss-nsamemean) = sqrt(cond);
    end
end

%% Fit using the Matlab fit function.
fitresult = fit(X,Y,fitmodel,opt);
delete(instructions);

%% Calculation of simulated data.
% Create a string containing the call of all coefficients.
coeffstr = [];
for i=1:ngauss
    coeffstr = [coeffstr 'fitresult.A' num2str(i) ',']; %#ok<AGROW>
end
coeffstr = [coeffstr 'fitresult.a0,fitresult.a1,'];
for i=1:ngauss
    if isempty(find(samemean==i, 1))
        coeffstr = [coeffstr 'fitresult.m' num2str(i) ',']; %#ok<AGROW>
    end
end
for i=1:ngauss
    coeffstr = [coeffstr 'fitresult.s' num2str(i) ',']; %#ok<AGROW>
end
coeffstr(end) = [];
% Call the model with the inverted coefficients.
eval(['model = fitmodel(' coeffstr ',X);']);
% Add the simulated data on the data plot.
hold on
plot(X,model,'r','linewidth',2)

%% Calculation of residuals.
residuals = Y-model;
shift = max(residuals)-min(Y);
plot(X,residuals-shift,'.-g')
legend('Data','Fit','Shifted residuals')
set(gca,'fontsize',fs)

%% Add lines to show position of gaussian functions.
axe = axis; %#ok<NASGU>
for i=1:ngauss
    if ~isempty(find(samemean==i, 1))
        j=samemeanas(samemean==i);
        eval(['line([fitresult.m' num2str(j) ' fitresult.m' num2str(j) ...
            '],[axe(3) axe(4)],''linestyle'',''--'',''color'','...
            '[0.5 0.5 0.5]);'])
    else
        eval(['line([fitresult.m' num2str(i) ' fitresult.m' num2str(i) ...
            '],[axe(3) axe(4)],''linestyle'',''--'',''color'','...
            '[0.5 0.5 0.5]);'])
    end
end

end