%% Cleaning workspace
clear all
close all
clc

%% Data
lambda0 = [24; 63.076; 137.3463; 153.0843; 173.6015]*1e-6;
nu0 = 299792458./lambda0;
cont = [0.792; 1.95; 1.32; 1.25; 1.20]*1e-17;
dcont = [0.05192; 0.43; 0.09; 0.11; 0.11]*1e-17;

%% Functions
% Planck function.
Bnu = fittype( @(T,x) ((2*6.62617e-34*x.^3)/(299792458^2)) ./ ...
    (exp((6.62617e-34*x)./(1.38066e-23*T))-1) );
% Modified Planck function.
Lnu = fittype( @(K,T,x) ...
    ((2*6.62617e-34*K*(x.^(1.8+3)))./(299792458^(1.8+2))) ./ ...
    (exp((6.62617e-34*x)./(1.38066e-23*T))-1));

%% Fit
% Planck.
% opt = fitoptions(Bnu);
% opt.robust = 'On';
% opt.Lower = 0;
% opt.Upper = 1e3;
% opt.StartPoint = 20;
% opt.DiffMaxChange = 1e-3;
% opt.DiffMaxChange = 10;
% opt.MaxFunEvals = 1000;
% opt.MaxIter = 1000;
% opt.Display = 'iter';
% opt.TolFun = 1e-50;
% opt.TolX = 1e-20;
% fitresult = fit(nu0,cont,Bnu,opt);
% T = coeffvalues(fitresult);
% plot(nu0,cont,'b',nu0,Bnu(T,nu0),'r')

%
opt = fitoptions(Lnu);
opt.Lower = [-Inf 0];
opt.Upper = [Inf Inf];
opt.StartPoint = [1 1500];
opt.DiffMinChange = 0;
opt.DiffMaxChange = 1;
opt.MaxFunEvals = 1000;
opt.MaxIter = 1000;
opt.Display = 'iter';
opt.TolFun = 1e-50;
opt.TolX = 1e-50;
fitresult = fit(nu0,cont,Lnu,opt);