clear all
close all
clc

%% Data
if ~exist('CarinaContinuum.mat','file')
    addpath '../Flux/'
    GetCarinaContinuum
end
clear all
load('CarinaContinuum.mat')
wvl = cell2mat(CarinaContinuum(2:end,1));
continuum = cell2mat(CarinaContinuum(2:end,2));
uncertainties = 0.01*cell2mat(CarinaContinuum(2:end,3)).*continuum;
% wvl = [wvl;24];
% continuum = [continuum;792];
% uncertainties = [uncertainties;51.92];

wvl = [wvl;250];
continuum = continuum*(9.4)^2/(pi*3.7/2*9.5/2);
uncertainties = uncertainties*(9.4)^2/(pi*3.7/2*9.5/2);
continuum = [continuum;602.2];
uncertainties = [uncertainties;224.7];
% wvl = [wvl;350];
% continuum = [continuum;196];
% uncertainties = [uncertainties;40];
% wvl = [wvl;24];
% continuum = [continuum;792];
% uncertainties = [uncertainties;51.92];
    
use_sahai = 1;
if use_sahai==1
    % Data from Sahai et al. (2012).
    l = 350; % micrometers.
    nu = 1e-9*299792448/(l*1e-6); % Ghz.
%     beamsize = 7.7*812/nu; % Size in arcsec, extrapolated from 7.7'' @ 812 Ghz.
%     beamsize = 19.2; % as from http://www.apex-telescope.org/bolometer/laboca/calibration/
%     dbeamsize = 0.3; % as
beamsize = 7.8;
dbeamsize = 0;
    wvl(length(wvl)+1) = l;
    continuum(length(wvl)) = 196 * 1e-9 ... % from mJy to MJy
        / (pi*(0.5*beamsize*pi/180/3600)^2) ... % from MJy to MJy/sr
        * (beamsize/2)^2/(3.7/2*9.5/2);% Normalized
    uncertainties(length(wvl)) = continuum(length(wvl))*sqrt((65/196)^2+4*(dbeamsize/beamsize)^2);
    clear('l','nu','beamsize')
end

% % PACS Photometry
% s1=2.77458; ds1=1.66571; n1=25;
% s2=8.12178; ds2=2.84987; n2=81;
% mf = (s2-s1)/(n2-n1); dmf = sqrt(ds1^2+ds2^2)/(n2-n1);
% mcbff = s1/n1-mf; dmcbff = sqrt((ds1/n1)^2+dmf^2);
% pix=4*(pi/180/3600)^2;
% mcbff_70 = 1e-6*mcbff/pix;
% dmcbff_70 = 1e-6*dmcbff/pix;
% s1=1.06429; ds1=1.03164; n1=9;
% s2=4.25430; ds2=2.0626; n2=42;
% mf = (s2-s1)/(n2-n1); dmf = sqrt(ds1^2+ds2^2)/(n2-n1);
% mcbff = s1/n1-mf; dmcbff = sqrt((ds1/n1)^2+dmf^2);
% pix=4*(pi/180/3600)^2;
% mcbff_160 = 1e-6*mcbff/pix;
% dmcbff_160 = 1e-6*dmcbff/pix;
% wvl(length(wvl)+1:length(wvl)+2) = [70 160];
% continuum(length(wvl)-1:length(wvl)) = [mcbff_70 mcbff_160];
% uncertainties(length(wvl)-1:length(wvl)) = [dmcbff_70 dmcbff_160];

% wvl_sup = 24;
% continuum_sup = 792;
% uncertainties_sup = 51.92;
wvl_sup = [24 870]';
% continuum_sup = [792 40*1e-9/(pi*(0.5*(17.3*352/(1e-9*299792458/(wvl_sup(2)*1e-6)))*pi/180/3600)^2)];
continuum_sup = [792 40*1e-9/(pi*(0.5*19*pi/180/3600)^2)*(19/2)^2/(3.7/2*9.5/2)]';
uncertainties_sup = [51.92 0]';

%% Figure 1
fig1 = figure;
set(fig1,'Units','Normalized')
set(fig1,'Position',[0 0 1 1])
hold on
errorbar(wvl,continuum,uncertainties,'k.')
errorbar(wvl_sup,continuum_sup,uncertainties_sup,'r.')
mks = 6;
plot(wvl,continuum,'ko','markersize',mks,'markerfacecolor','k')
plot(wvl_sup,continuum_sup,'rv','markersize',mks,'markerfacecolor','r')
set(gca,'YScale','log')
% axe = [0 500 10 1e4];
axe = [0 900 10 1e4];
axis(axe)

%% Fit
% Double SED function.
Lnu = fittype( @(tau_nu0_1,T_1,tau_nu0_2,T_2,x) ...
    ((tau_nu0_1*2*6.62617e-34*x.^(3+1.8)) ...
    / ((299792458^2)*(299792458/250e-6)^1.8) ) ./ ... 
    (exp((6.62617e-34*x)./(1.38066e-23*T_1))-1) + ... % SED1
    ((tau_nu0_2*2*6.62617e-34*x.^(3+1.8)) ...
    / ((299792458^2)*(299792458/250e-6)^1.8) ) ./ ...
    (exp((6.62617e-34*x)./(1.38066e-23*T_2))-1) ... % SED2
    );
% Fit options.
opt = fitoptions(Lnu);
opt.Lower = [0 0 0 0];
opt.Upper = [Inf 500 Inf 100]; % 75 from the variable test
opt.StartPoint = [1e-4 17 1e-5 60];
opt.Weights = 1./uncertainties;
opt.DiffMinChange = 0;
opt.DiffMaxChange = 1;
opt.MaxFunEvals = 2000;
opt.MaxIter = 2000;
opt.Display = 'iter';
opt.TolFun = 1e-50;
opt.TolX = 1e-50;
fitresult = fit(299792458./(wvl*1e-6),continuum*1e-20,Lnu,opt); % In SI.

%% Add fit in Figure 1
wvl_model = 0.1:0.1:900;
SED1 = 1e20 * ...
    Lnu(fitresult.tau_nu0_1,fitresult.T_1,0,0,299752458./(wvl_model*1e-6));
SED2 = 1e20 * ...
    Lnu(0,0,fitresult.tau_nu0_2,fitresult.T_2,299752458./(wvl_model*1e-6));
SED = SED1+SED2;
plot(wvl_model,SED1,'k--')
plot(wvl_model,SED2,'k--')
plot(wvl_model,SED,'b','linewidth',2)

%% Mass
% C = coeffvalues(fitresult);
% Cint = confint(fitresult);
% dC = (Cint(2,:)-C)/1.96;

% s0 = 1.14e-24; 
s0 = 2.32e-25; 
nH1 = fitresult.tau_nu0_1/s0;
% dnH1 = dC(1)/s0;
nH2 = fitresult.tau_nu0_2/s0;
% dnH2 = dC(1)/s0;

mu = 1.4;
mH = 1.67262178e-27; % Proton mass in kg.
D = 2.3e3; % in pc. 
% Pixel_as = 9.4;
% Pixel_as2UA_at_distD = D*tand(Pixel_as/3600)/tand(1/3600);
% UA2cm = 1.495978707e13;
% Pixel_cm = Pixel_as2UA_at_distD * UA2cm;
% Omega = Pixel_cm^2;
as2cm = inline('Dpc*tand(as/3600)/tand(1/3600)*1.495978707e13','as','Dpc');
Omega = pi*as2cm(9.5/2,D)*as2cm(3.7/2,D);

M1 = mu * mH * nH1 * Omega;
% dM1 = mu * mH * dnH1 * Omega;
M1_sunmass = M1 / 1.9891e30;
% dM1_sunmass = dM1 / 1.9891e30;
M2 = mu * mH * nH2 * Omega;
% dM2 = mu * mH * dnH2 * Omega;
M2_sunmass = M2 / 1.9891e30;
% dM2_sunmass = dM2 / 1.9891e30;
M = M1+M2;
% dM = sqrt(dM1^2+dM2^2);
M_sunmass = M1_sunmass+M2_sunmass
% dM_sunmass = sqrt(M1_sunmass^2+dM2_sunmass^2);

%%
Lnu1 = fittype( @(tau_nu0,T,x) ...
    ((tau_nu0*2*6.62617e-34*x.^(3+1.8)) ...
    / ((299792458^2)*(299792458/250e-6)^1.8) ) ./ ... 
    (exp((6.62617e-34*x)./(1.38066e-23*T))-1) );
% Fit options.
opt1 = fitoptions(Lnu1);
opt1.Lower = [0 0];
opt1.Upper = [Inf 500];
opt1.StartPoint = [1e-5 60];
opt1.Weights = 1./uncertainties;
opt1.DiffMinChange = 0;
opt1.DiffMaxChange = 1;
opt1.MaxFunEvals = 2000;
opt1.MaxIter = 2000;
opt1.Display = 'iter';
opt1.TolFun = 1e-50;
opt1.TolX = 1e-50;
fitresult1 = fit(299792458./(wvl*1e-6),continuum*1e-20,Lnu1,opt1); % In SI.

wvl_model = 0.1:0.1:900;
SEDonlyone = 1e20 * ...
    Lnu1(fitresult1.tau_nu0,fitresult1.T,299752458./(wvl_model*1e-6));
plot(wvl_model,SEDonlyone,'r--','linewidth',1)

% %% 
% fig2 = figure;
% % evol_tau_nu0 = 0.01*fitresult.tau_nu0_1:0.001*fitresult.tau_nu0_1:1*fitresult.tau_nu0_1;
% % evol_T = 0.10*fitresult.T_1:0.1*fitresult.T_1:10*fitresult.T_1;
% % evol_tau_nu0 = logspace(log10(0.001*fitresult.tau_nu0_1),log10(100*fitresult.tau_nu0_1),100);
% % evol_T = logspace(log10(0.001*fitresult.T_1),log10(100*fitresult.T_1),100);
% logfactor = 1;
% factor = linspace(-logfactor,logfactor,101);
% evol_tau_nu0 = fitresult.tau_nu0_2*10.^factor;
% evol_T = fitresult.T_2*10.^factor;
% [X,Y] = meshgrid(evol_tau_nu0,evol_T);
% SEDcube = zeros(size(X));
% for i=1:size(X,1)
% for j=1:size(X,2)
%     SEDcube(i,j) = 1e20 * ...
%         Lnu(fitresult.tau_nu0_1,fitresult.T_1,X(i,j),Y(i,j),299752458./(24*1e-6));
% end
% end
% SEDcube_norm = log10(SEDcube/continuum_sup);
% imagesc(factor,factor,SEDcube_norm,'AlphaData',SEDcube_norm<0)
% set(gca,'ydir','normal')
% xlabel('log10(tau_nu0 norm)','interpreter','none')
% ylabel('log10(T norm)')
% colorbar
% caxe = caxis;
% caxe(2)=0;
% caxis(caxe)
% 
% xt = [];
% for i=-logfactor:logfactor-1
%     xt = [xt 1*10^i 5*10^i]; %#ok<AGROW>
% end
% xt = [xt 10^logfactor];
% yt = xt;
% 
% xtl = num2str(xt');
% ytl = num2str(yt'); 
% 
% set(gca,'XTick',log10(xt))
% set(gca,'YTick',log10(yt))
% set(gca,'XTickLabel',xtl)
% set(gca,'YTickLabel',ytl)
% 
% % xtl = get(gca,'XTickLabel');
% % xtl = num2str(10.^str2num(xtl),'%4.2f'); %#ok<ST2NM>
% % set(gca,'XTickLabel',xtl);
% % ytl = get(gca,'YTickLabel');
% % ytl = num2str(10.^str2num(ytl),'%4.2f'); %#ok<ST2NM>
% % set(gca,'YTickLabel',ytl);
% % xlabel('normalized tau_nu0','interpreter','none')
% % ylabel('normalized temperature')

%%
% fig3 = figure;
% set(fig3,'Units','Normalized')
% set(fig3,'Position',[0 0 1 1])
% hold on
% percent = 20;
% mask = [0 0 0 1];
% mincurve = 1e20 * Lnu((1-percent*mask(1)/100)*fitresult.tau_nu0_1,(1-percent*mask(2)/100)*fitresult.T_1,...
%     (1-percent*mask(3)/100)*fitresult.tau_nu0_2,(1-percent*mask(4)/100)*fitresult.T_2,...
%     299752458./(wvl_model*1e-6));
% maxcurve = 1e20 * Lnu((1+percent*mask(1)/100)*fitresult.tau_nu0_1,(1+percent*mask(2)/100)*fitresult.T_1,...
%     (1+percent*mask(3)/100)*fitresult.tau_nu0_2,(1+percent*mask(4)/100)*fitresult.T_2,...
%     299752458./(wvl_model*1e-6));
% ar1 = area(wvl_model,maxcurve);
% set(ar1,'FaceColor',[0.8 0.8 0.8],'EdgeColor','b')
% ar2 = area(wvl_model,mincurve);
% set(ar2,'FaceColor','w','EdgeColor','b')
% errorbar(wvl,continuum,uncertainties,'k.')
% errorbar(wvl_sup,continuum_sup,uncertainties_sup,'r.')
% mks = 6;
% plot(wvl,continuum,'ko','markersize',mks,'markerfacecolor','k')
% plot(wvl_sup,continuum_sup,'rv','markersize',mks,'markerfacecolor','r')
% set(gca,'YScale','log')
% axe = [0 900 10 3e3];
% axis(axe)
% wvl_model = 0.1:0.1:400;
% 
% SED1 = 1e20 * ...
%     Lnu(fitresult.tau_nu0_1,fitresult.T_1,0,0,299752458./(wvl_model*1e-6));
% SED2 = 1e20 * ...
%     Lnu(0,0,fitresult.tau_nu0_2,fitresult.T_2,299752458./(wvl_model*1e-6));
% SED = SED1+SED2;
% plot(wvl_model,SED1,'k--')
% plot(wvl_model,SED2,'k--')
% plot(wvl_model,SED,'b','linewidth',2)

%%
p0 = coeffvalues(fitresult);
x = 299792458./([wvl;wvl_sup]*1e-6);
ym = [continuum-uncertainties;zeros(size(continuum_sup))]*1e-20;
yp = [continuum+uncertainties;continuum_sup+uncertainties_sup]*1e-20;
log_factors = [2 1 1 1];
nok = 1000; 
plower = [0 10 0 0];
pupper = [Inf Inf Inf Inf];
% tic
% [p, uncert, randparam] = MonteCarloUncert(x,ym,yp,Lnu,p0,...
%     log_factors,nok,plower,pupper);
% toc
pwdvar = pwd;
load('MonteCarloResult_1000.mat','p','uncert','randparam');
wvl2 = 10:1:900;
fig4 = figure;
hold on
for it = 1:nok
    plot(wvl2,1e20*Lnu(randparam(1,it),randparam(2,it),randparam(3,it),randparam(4,it),299792458./(wvl2*1e-6)))
end
errorbar(wvl,continuum,uncertainties,'k.')
errorbar(wvl_sup,continuum_sup,uncertainties_sup,'r.')
mks = 6;
plot(wvl,continuum,'ko','markersize',mks,'markerfacecolor','k')
plot(wvl_sup,continuum_sup,'rv','markersize',mks,'markerfacecolor','r')
set(gca,'YScale','log')
axis(axe)

fig5 = figure;
titlestr = {'tau_nu0_1 (cold)','T_1 (cold)',...
    'tau_nu0_2 (hot)','T_2 (hot)'};
for it=1:4
    subplot(2,2,it)
    hist(randparam(it,:),50)
    title(titlestr(it),'interpreter','none')
end

fig6 = figure;
nH_cold = randparam(1,:)/s0;
nH_hot = randparam(3,:)/s0;
M_cold_sunmass = mu * mH * nH_cold * Omega / 1.9891e30;
M_hot_sunmass = mu * mH * nH_hot * Omega / 1.9891e30;
subplot(2,1,1)
hist(M_cold_sunmass,50)
title('cold')
subplot(2,1,2)
hist(M_hot_sunmass,50)
title('hot')

%%
Param = M_cold_sunmass;
model = zeros(length(wvl),nok);
for it=1:nok
    model(:,it) = 1e20*Lnu(randparam(1,it),randparam(2,it),randparam(3,it),randparam(4,it),299792458./(wvl*1e-6));
end
% chi2 = sum(abs(model-repmat(continuum,[1 nok]))./repmat(uncertainties,[1 nok]));
% chi2 = 100*sum(abs(model-repmat(continuum,[1 nok]))./repmat(uncertainties,[1 nok]))/length(continuum);
chi2 = sum(( (model-repmat(continuum,[1 nok]))./repmat(uncertainties,[1 nok]) ).^2,1);
% chi2 = sum(abs(model-repmat(continuum,[1 nok])));
chi2norm = chi2/max(chi2(:));

nbars = 50;
edges = linspace(min(Param(:)),1.0001*max(Param(:)),51);
centres = zeros(nbars,1);
barchi2 = zeros(nbars,1);
for it=1:nbars
    ind = find(Param>=edges(it) & Param<edges(it+1));
    centres(it) = mean([edges(it) edges(it+1)]);
    barchi2(it) = mean(chi2norm(ind));
end

col = bone(nbars); % start with the dark color.
[~,indcol]=sort(barchi2,'ascend'); % start with the minimum chi2 (which will be dark so).
N = histc(Param,edges);
N(end) = [];
width = edges(2)-edges(1);
fig7=figure;
hold on
for it=1:nbars
    bar(centres(it),N(it),width,'facecolor',col(indcol(it),:))
end

%%
%{
nbars = 50;
Param1 = M_cold_sunmass;
fig8 = figure;
subplot(2,1,1)
[~, X] = hist(Param1,nbars);
width = X(2)-X(1);
edges = [X(1)-width/2;X(:)+width/2];
hist(Param1,nbars)
g = ginput(2);
gmin = g(1,1);
gmax = g(2,1);
gmin = edges(find(edges<gmin,1,'last'));
gmax = edges(find(edges>gmax,1,'first'));
ind = find(Param1>=gmin & Param1<=gmax);
N = histc(Param1(ind),edges);
hold on
for it=1:nbars
    bar(X(it),N(it),width,'r')
end

Param2 = randparam(2,:);
subplot(2,1,2)
[~, X] = hist(Param2,nbars);
hist(Param2,nbars)
width = X(2)-X(1);
edges = [X(1)-width/2;X(:)+width/2];
hold on
N = histc(Param2(ind),edges);
N(end) = [];
for it=1:nbars
    bar(X(it),N(it),width,'r')
end
%}

%%
fig9 = figure;
fs = 13;
plot(M_cold_sunmass,chi2,'ko','MarkerFaceColor','k')
xlabel('Mass of the disk (solar masses)','fontsize',fs)
ylabel('Normalized mean deviation (%)','fontsize',fs)
M_cold_sunmass_best = M_cold_sunmass(min(chi2(:))==chi2);
axe = axis;
line([M_cold_sunmass_best M_cold_sunmass_best],...
    axe(3:4),'color','r','linewidth',2)
gcapos = get(gca,'position');
text(M_cold_sunmass_best,axe(3)-(axe(4)-axe(3))/100,num2str(M_cold_sunmass_best,'%3.2f'),...
    'VerticalAlignment','Top','HorizontalAlignment','Center',...
    'color','r','fontsize',fs,'fontweight','bold')
% print('-dpng','-r500','chi2.png')

%%
fig10 = figure;
vec_tau = log10(logspace(log10(0.001),...
    log10(0.1),500));
vec_T = linspace(10,...
    20,400);
[grid_tau, grid_T] = meshgrid(vec_tau,vec_T);
grid_model = zeros([size(grid_tau) length(wvl)]);
grid_chi2 = zeros(size(grid_tau));
grid_sup = zeros([size(grid_tau) length(wvl_sup)]);
mask = zeros(size(grid_tau));
[~, indbestmodel] = min(chi2);
besttaucold = randparam(1,indbestmodel);
bestTcold = randparam(2,indbestmodel);
besttauhot = randparam(3,indbestmodel);
bestThot = randparam(4,indbestmodel);
for itl=1:size(grid_model,1)
    for itc=1:size(grid_model,2)
        grid_model(itl,itc,:) = 1e20*Lnu(10^grid_tau(itl,itc),grid_T(itl,itc),...
            besttauhot,bestThot,...
            299792458./(wvl*1e-6));
        grid_chi2(itl,itc) = ...
            sum(( (squeeze(grid_model(itl,itc,:))-continuum)./uncertainties ).^2,1);
%             100 * sum( abs(squeeze(grid_model(itl,itc,:)) - continuum) ./ ...
%             uncertainties ) / length(continuum);
        grid_sup(itl,itc,:) = 1e20*Lnu(10^grid_tau(itl,itc),grid_T(itl,itc),...
            besttauhot,bestThot,...
            299792458./(wvl_sup*1e-6));
        if  squeeze(grid_sup(itl,itc,:)) < continuum_sup+uncertainties_sup
            mask(itl,itc) = 1;
        end
    end
end

%%
% imagesc(vec_tau,vec_T,log10(grid_chi2/min(grid_chi2(:))),'AlphaData',(mask+2)/3)
mask(mask<1) = 0;
imagesc(vec_tau,vec_T,log10(grid_chi2/min(grid_chi2(:))))
cb = colorbar;
colormap(jet(100));

% ctick = 20:10:100;
% ctickl = num2str(ctick(:));
% ctick = log10(ctick);
% caxe = [ctick(1) ctick(end)];
% caxis(caxe);
% set(cb,'YTick',ctick)
% set(cb,'YTickLabel',ctickl)
caxis([0 2])

set(gca,'Ydir','Normal')

xtick = [1e-3:1e-3:1e-2 2e-2:1e-2:1e-1];
x_nH = xtick/s0;
x_M = mu * mH * (x_nH+besttauhot/s0) * Omega / 1.9891e30;
xtickl = repmat(' ',[length(xtick) 13]);
indkeep = 1:9:length(xtick);
xtickl(indkeep,:) = [num2str(xtick(indkeep)','%5.0e') ...
    repmat(' (',[length(indkeep) 1]) ...
    num2str(x_M(indkeep)','%4.2f') ...
    repmat(')',[length(indkeep) 1])];
set(gca,'XTick',log10(xtick))
set(gca,'XTickLabel',xtickl)

xlabel('\tau_{\nu} (disk mass  [sollar masses])','fontsize',fs)
ylabel('T [K]','fontsize',fs)
title('Normalized mean deviation (%)','fontsize',fs)

[~, indmin] = min(grid_chi2(:));
besttau = 10^grid_tau(indmin);
bestT = grid_T(indmin);
bestM = mu * mH * (besttau+besttauhot)/s0 * Omega / 1.9891e30;
axe = axis;
line([log10(besttau) log10(besttau)],[axe(3) bestT],'color','w')
line([axe(1) log10(besttau)],[bestT bestT],'color','w')

text(axe(1),axe(3),...
    ['  T = ' num2str(bestT,'%4.2f') ' K' char(10) ...
    '  \tau_\nu = ' num2str(besttau,'%8.2e') ...
    ' --> M_d = ' num2str(bestM,'%4.2f') ' M_0' ],...
    'VerticalAlignment','Bottom','HorizontalAlignment','Left',...
    'color','w','fontsize',fix(0.8*fs))
% print('-dpng','-r500','degen_v2.png')
