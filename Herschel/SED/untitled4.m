%% Cleaning workspace
clear all
close all
clc

%% Data
% lambda0 = [24; 63.076; 137.3463; 153.0843; 173.6015]*1e-6;
% n = 299792458./lambda0;
% P = [0.792; 1.95; 1.32; 1.25; 1.20]*1e-17;

lambda0 = [63.1946; 137.285; 153.3358; 173.6944; 350]*1e-6;
lambda0plot = [24; 63.1946; 137.285; 153.3358; 173.6944; 350]*1e-6;
n0 = 299792458./lambda0;
nplot = 299792458./lambda0plot;
beam = 800/(299792458/350e-6*1e-9)*7.7;
pix = pi*(beam/3600 * pi/180)^2;
P = [1528; 360.7; 286; 248; 196*1e-9/pix]*1e-20;
Pplot = [792; 1528; 360.7; 286; 248; 196*1e-9/pix]*1e-20;
dcont = P*0.3;
dcontplot = Pplot*0.3;
% dcont = [30; 0.8; 8; 5; 65]*1e-20;
% dcontplot = [51.92; 30; 0.8; 8; 5; 65]*1e-20;

% lambda0 = [63.1946; 137.285; 153.3358; 173.6944]*1e-6;
% lambda0plot = [24; 63.1946; 137.285; 153.3358; 173.6944]*1e-6;
% n0 = 299792458./lambda0;
% nplot = 299792458./lambda0plot;
% P = [1528; 360.7; 286; 248]*1e-20;
% Pplot = [792; 1528; 360.7; 286; 248]*1e-20;
% dcont = [30; 0.8; 8; 5]*1e-20;
% dcontplot = [51.92; 30; 0.8; 8; 5]*1e-20;

%% Functions
% Lnu = fittype( @(tau_nu0,T,x) ( (tau_nu0*2*6.62617e-34*x.^(3+1.8)) / ((299792458^2)*(299792458/250e-6)^1.8) ) ./ ...
%     (exp((6.62617e-34*x)./(1.38066e-23*T))-1) );
Lnu = fittype( @(tau_nu0_1,T_1,tau_nu0_2,T_2,x) ...
    ((tau_nu0_1*2*6.62617e-34*x.^(3+1.8)) / ((299792458^2)*(299792458/250e-6)^1.8) ) ./ ...
    (exp((6.62617e-34*x)./(1.38066e-23*T_1))-1) + ...
    ((tau_nu0_2*2*6.62617e-34*x.^(3+1.8)) / ((299792458^2)*(299792458/250e-6)^1.8) ) ./ ...
    (exp((6.62617e-34*x)./(1.38066e-23*T_2))-1) ...
    );
opt = fitoptions(Lnu);
% opt.Lower = [-Inf 0];
% opt.Upper = [Inf 500];
% opt.StartPoint = [1 50];
opt.Lower = [-Inf 0 -Inf 0];
opt.Upper = [Inf 500 Inf 500];
opt.StartPoint = [1e-4 17 1e-3 60];
opt.Weights = 1./dcont;
opt.DiffMinChange = 0;
opt.DiffMaxChange = 1;
opt.MaxFunEvals = 1000;
opt.MaxIter = 1000;
opt.Display = 'iter';
opt.TolFun = 1e-50;
opt.TolX = 1e-50;
fitresult = fit(n0(:),P(:),Lnu,opt);

%%
close all
mks = 8;
figure
hold on
l = [15:0.1:400]*1e-6; %#ok<NBRAK>
c = 299792458;
n = c./l;

plot(lambda0*1e6,P*1e20,'ko','markersize',mks,'markerfacecolor','k')
plot(lambda0plot(1)*1e6,Pplot(1)*1e20,'rv','markersize',mks,'markerfacecolor','r')
SED1 = Lnu(fitresult.tau_nu0_1,fitresult.T_1,0,0,n);
SED2 = Lnu(fitresult.tau_nu0_2,fitresult.T_2,0,0,n);
plot(l*1e6,(SED1+SED2)*1e20,'b','linewidth',2)
plot(l*1e6,SED1*1e20,'k--')
plot(l*1e6,SED2*1e20,'k--')
% SED = Lnu(fitresult.tau_nu0,fitresult.T,n);
% plot(l*1e6,SED*1e20,'b','linewidth',2)
dC = confint(fitresult);
C = coeffvalues(fitresult);
uncert = (dC(2,:)-C)/1.96;
% % SEDup = Lnu(fitresult.tau_nu0+uncert(1),fitresult.T,n);
% % plot(l*1e6,SEDup*1e20,'r','linewidth',2)
% % SEDlo = Lnu(fitresult.tau_nu0-uncert(1),fitresult.T,n);
% % plot(l*1e6,SEDlo*1e20,'r','linewidth',2)
% % SEDup = Lnu(fitresult.tau_nu0,fitresult.T+uncert(2),n);
% % plot(l*1e6,SEDup*1e20,'r','linewidth',2)
% % SEDlo = Lnu(fitresult.tau_nu0,fitresult.T-uncert(2),n);
% % plot(l*1e6,SEDlo*1e20,'r','linewidth',2)
% SEDup = Lnu(fitresult.tau_nu0+uncert(1),fitresult.T+uncert(2),n);
% % plot(l*1e6,SEDup*1e20,'r','linewidth',2)
% SEDlo = Lnu(fitresult.tau_nu0-uncert(1),fitresult.T-uncert(2),n);
% % plot(l*1e6,SEDlo*1e20,'r','linewidth',2)
% confplot(l*1e6,SED*1e20,SEDlo*1e20,SEDup*1e20)

myerrorbar(lambda0*1e6,P*1e20,dcont,'color','k')
myerrorbar(lambda0plot(1)*1e6,Pplot(1)*1e20,dcontplot(1),'color','r')
set(gca,'yscale','log')

fs = 12;
xlabel('Wavelength (\mum)','fontsize',fs)
ylabel('Spectral radiance (MJy/sr)','fontsize',fs)
set(gca,'fontsize',fs)

% %%
% close all
% mks = 8;
% hold on
% % plot(n,P,'ko','markersize',mks,'markerfacecolor','k')
% % plot(nplot(1),Pplot(1),'rv','markersize',mks,'markerfacecolor','r')
% % plot(linspace(min(nplot(:)),max(nplot(:)),150),...
% %     Bnu(fitresult.K,fitresult.beta,fitresult.T,linspace(min(nplot(:)),...
% %     max(nplot(:)),150)),'b','linewidth',2)
% % 
% % myerrorbar(n,P,dcont,'color','k')
% % myerrorbar(nplot(1),Pplot(1),dcontplot(1),'color','r')
% % set(gca,'yscale','log')
% l = [15:0.1:200]*1e-6; %#ok<NBRAK>
% c = 299792458;
% n = c./l;
% 
% plot(lambda0*1e6,P*1e20,'ko','markersize',mks,'markerfacecolor','k')
% plot(lambda0plot(1)*1e6,Pplot(1)*1e20,'rv','markersize',mks,'markerfacecolor','r')
% plot(l*1e6,Lnu(fitresult.tau_nu0,fitresult.T,n)*1e20,'b','linewidth',2)
% 
% myerrorbar(lambda0*1e6,P*1e20,dcont,'color','k')
% myerrorbar(lambda0plot(1)*1e6,Pplot(1)*1e20,dcontplot(1),'color','r')
% set(gca,'yscale','log')
% 
% fs = 12;
% xlabel('Wavelength (\mum)','fontsize',fs)
% ylabel('Spectral radiance (MJy/sr)','fontsize',fs)
% set(gca,'fontsize',fs)

%%
G0 = 2.2e4;
a = 0.1; 
Td = 33.5*(1/a)^0.2*(G0/1e4)^0.2; %0.2 = 1/5, normalement 1/(4+beta).
G0est = (fitresult.T/33.5/(1/a)^0.2)^5*1e4;

%%
l = [10:0.1:1000]*1e-6; %#ok<NBRAK>
c = 299792458;
n = c./l;
Int = abs(trapz(n,Lnu(fitresult.tau_nu0,fitresult.T,n)));
disp(['Integrated spectral radiance : ' num2str(Int*1e4) ' E-04 W/m²/sr.']);

%% nH
nu0 = 299792458/250e-6;
s0 = 1.14e-24; 
Planck = fittype( @(T,x) ((2*6.62617e-34*x.^3)/(299792458^2)) ./ ...
    (exp((6.62617e-34*x)./(1.38066e-23*T))-1) );
tau_nu0 = Lnu(fitresult.tau_nu0,fitresult.T,nu0)./Planck(fitresult.T,nu0);
nH = tau_nu0/s0;

%% nH
s0 = 1.14e-24; 
tau_nu0 = fitresult.tau_nu0;
nH = tau_nu0/s0;

%% Conversion mean-max
factormean2max = 83.978/15.4319; % IRAC 8um.
% factormean2max = 8.00;
nHcorr = nH*factormean2max;

%d Dimension and distance
L = 9.5; % in as.
l = 3.7; % in as.
D = 2.3e3; % in pc.

% 1 pc = 1AU/tan(1"), 1 AU = 1.495978707e13 cm.
dL = (D*tand(L/3600)/tand(1/3600))*1.495978707e13;
dl = (D*tand(l/3600)/tand(1/3600))*1.495978707e13;

density = nHcorr/dl;

% nHmax = density*dL or :
nHmax = nHcorr*dL/dl;
Av = 0.5*nHmax/2.5e21;

%% Mass
mu = 1.4;
mH = 1.67262178e-27; % Proton mass in kg.
D = 2.3e3; % in pc. 
Pixel_as = 9.4;
Pixel_as2UA_at_distD = D*tand(Pixel_as/3600)/tand(1/3600);
UA2cm = 1.495978707e13;
Pixel_cm = Pixel_as2UA_at_distD * UA2cm;
Omega = Pixel_cm^2;
M = mu * mH * nH * Omega;

M_sunmass = M / 1.9891e30;

%% Mass 2
mu = 1.4;
mH = 1.67262178e-27; % Proton mass in kg.
c = 299752458; %m/s
nu = 330.588e9; %Hz
h = 6.62606957e-34; %J.s
k = 1.3806488e-23; %J/K
Cnu = h*nu/k;
Tbb = 2.7; %K;
f2 = 1/(exp(Cnu/Tbb)-1); % Second fraction.
Tb12C = 1.1; %K;
Tex = Cnu / log((Cnu/(Tb12C+f2*Cnu))+1);

Aul = 2.19e-6;
LineFlux = 0.5*sqrt(pi/log(2))*0.4*2.2e3; %K.m.s^-1
LineFlux = LineFlux*17.8/18.6; % Correction for beam size in 12 CO vs 13 CO.
Nu = (8*pi*k*nu^2/(h*c^3*Aul))*LineFlux; %m^-2
Nu = Nu*1e-4; %cm^-2

gu = 14;
Z = 42;
Eup = 32;
N = Nu*Z/gu/exp(-Eup/(Tex));

Nh2 = 7e5*N;
Nh = 2*Nh2;

% Mass
mu = 1.4;
mH = 1.67262178e-27; % Proton mass in kg.
D = 2.3e3; % in pc. 

as = 17.8;
as2cm = D*tand(as/3600)/tand(1/3600)*1.495978707e13;
Omega = pi*as2cm^2;
M = mu * mH * Nh * Omega;
M_sunmass = M / 1.9891e30;