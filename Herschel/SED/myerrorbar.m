function myerrorbar(x,y,e,varargin)

ymin = y - e;
ymax = y + e;

for i = 1:length(x)
    line([x(i) x(i)],[ymin(i) ymax(i)],varargin{:});
end

end