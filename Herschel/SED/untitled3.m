%% Cleaning workspace
clear all
close all
clc

%% Data
% lambda0 = [24; 63.076; 137.3463; 153.0843; 173.6015]*1e-6;
% n = 299792458./lambda0;
% P = [0.792; 1.95; 1.32; 1.25; 1.20]*1e-17;
lambda0 = [63.1946; 137.285; 153.3358; 173.6944]*1e-6;
lambda0plot = [24; 63.1946; 137.285; 153.3358; 173.6944]*1e-6;
n0 = 299792458./lambda0;
nplot = 299792458./lambda0plot;
P = [1528; 360.7; 286; 248]*1e-20;
Pplot = [792; 1528; 360.7; 286; 248]*1e-20;
dcont = [30; 0.8; 8; 5]*1e-20;
dcontplot = [51.92; 30; 0.8; 8; 5]*1e-20;


%% Functions
Bnu = fittype( @(K,beta,T,x) ((2*K*6.62617e-34*x.^(3+0*beta+1.8))/(299792458^(2+0*beta+1.8))) ./ ...
    (exp((6.62617e-34*x)./(1.38066e-23*T))-1) );
opt = fitoptions(Bnu);
opt.Lower = [-Inf -Inf 0];
opt.Upper = [Inf Inf 500];
opt.StartPoint = [1 1.8 110];
opt.Weights = 1./dcont;
opt.DiffMinChange = 0;
opt.DiffMaxChange = 1;
opt.MaxFunEvals = 1000;
opt.MaxIter = 1000;
opt.Display = 'iter';
opt.TolFun = 1e-50;
opt.TolX = 1e-50;
fitresult = fit(n0(:),P(:),Bnu,opt);

%%
close all
mks = 8;
hold on
% plot(n,P,'ko','markersize',mks,'markerfacecolor','k')
% plot(nplot(1),Pplot(1),'rv','markersize',mks,'markerfacecolor','r')
% plot(linspace(min(nplot(:)),max(nplot(:)),150),...
%     Bnu(fitresult.K,fitresult.beta,fitresult.T,linspace(min(nplot(:)),...
%     max(nplot(:)),150)),'b','linewidth',2)
% 
% myerrorbar(n,P,dcont,'color','k')
% myerrorbar(nplot(1),Pplot(1),dcontplot(1),'color','r')
% set(gca,'yscale','log')
l = [15:0.1:200]*1e-6; %#ok<NBRAK>
c = 299792458;
n = c./l;

plot(lambda0*1e6,P*1e20,'ko','markersize',mks,'markerfacecolor','k')
plot(lambda0plot(1)*1e6,Pplot(1)*1e20,'rv','markersize',mks,'markerfacecolor','r')
plot(l*1e6,Bnu(fitresult.K,fitresult.beta,fitresult.T,n)*1e20,'b','linewidth',2)

myerrorbar(lambda0*1e6,P*1e20,dcont,'color','k')
myerrorbar(lambda0plot(1)*1e6,Pplot(1)*1e20,dcontplot(1),'color','r')
set(gca,'yscale','log')

fs = 12;
xlabel('Wavelength (\mum)','fontsize',fs)
ylabel('Spectral radiance (MJy/sr)','fontsize',fs)
set(gca,'fontsize',fs)

%%
G0 = 2.2e4;
a = 0.1; 
Td = 33.5*(1/a)^0.2*(G0/1e4)^0.2; %0.2 = 1/5, normalement 1/(4+beta).
G0est = (fitresult.T/33.5/(1/a)^0.2)^5*1e4;

%%
l = [10:0.1:1000]*1e-6; %#ok<NBRAK>
c = 299792458;
n = c./l;
Int = abs(trapz(n,Bnu(fitresult.K,fitresult.beta,fitresult.T,n)));
disp(['Integrated spectral radiance : ' num2str(Int*1e4) ' E-04 W/m²/sr.']);

%% nH
c = 299792458;
l0 = 250e-6;
nu0 = c./l0;
s0 = 1.14e-24; 
tau_nu0 = fitresult.K*c/(l0^(fitresult.beta+2))
tau_nu0 = fitresult.K*nu0^(fitresult.beta+2)/c^(fitresult.beta+1)
nH = tau_nu0/s0

%% nH
Planck = fittype( @(T,x) ((2*6.62617e-34*x.^3)/(299792458^2)) ./ ...
    (exp((6.62617e-34*x)./(1.38066e-23*T))-1) );
tau_nu0 = Bnu(fitresult.K,fitresult.beta,fitresult.T,n0)./Planck(fitresult.T,nu0);
nH = tau_nu0/s0
