%% Cleaning workspace
clear all
close all
clc

%% Data
lambda0 = [24; 63.076; 137.3463; 153.0843; 173.6015]*1e-6;
% nu0 = 299792458./lambda0;
cont = [0.792; 1.95; 1.32; 1.25; 1.20]*1e-17;
dcont = [0.05192; 0.43; 0.09; 0.11; 0.11]*1e-17;

%% Functions
% Planck function.
load test.mat;
Bnu = fittype( @(T,x) ((2*6.62617e-34*x.^3)/(299792458^2)) ./ ...
    (exp((6.62617e-34*x)./(1.38066e-23*T))-1) );
opt = fitoptions(Bnu);
opt.Lower = [-Inf];
opt.Upper = [Inf];
opt.StartPoint = [100];
opt.DiffMinChange = 0;
opt.DiffMaxChange = 1;
opt.MaxFunEvals = 1000;
opt.MaxIter = 1000;
opt.Display = 'iter';
opt.TolFun = 1e-50;
opt.TolX = 1e-50;
fitresult = fit(n(:),P(:),Bnu,opt);
%% Cleaning workspace
clear all
close all
clc

%% Data
lambda0 = [24; 63.076; 137.3463; 153.0843; 173.6015]*1e-6;
% nu0 = 299792458./lambda0;
cont = [0.792; 1.95; 1.32; 1.25; 1.20]*1e-17;
dcont = [0.05192; 0.43; 0.09; 0.11; 0.11]*1e-17;

%% Functions
% Planck function.
load test.mat;
Bnu = fittype( @(T,x) ((2*6.62617e-34*x.^3)/(299792458^2)) ./ ...
    (exp((6.62617e-34*x)./(1.38066e-23*T))-1) );
opt = fitoptions(Bnu);
opt.Lower = [-Inf];
opt.Upper = [Inf];
opt.StartPoint = [100];
opt.DiffMinChange = 0;
opt.DiffMaxChange = 1;
opt.MaxFunEvals = 1000;
opt.MaxIter = 1000;
opt.Display = 'iter';
opt.TolFun = 1e-50;
opt.TolX = 1e-50;
fitresult = fit(n(:),P(:),Bnu,opt);

%% Cleaning workspace
clear all
close all
clc

%% Data
lambda0 = [24; 63.076; 137.3463; 153.0843; 173.6015]*1e-6;
% nu0 = 299792458./lambda0;
cont = [0.792; 1.95; 1.32; 1.25; 1.20]*1e-17;
dcont = [0.05192; 0.43; 0.09; 0.11; 0.11]*1e-17;

%% Functions
% Planck function.
load test.mat;
% lambda0 = [24; 63.076; 137.3463; 153.0843; 173.6015]*1e-6;
% n = 299792458./lambda0;
% P = [0.792; 1.95; 1.32; 1.25; 1.20]*1e-17;
P = (1+0.1*rand(size(P))).*P;
n(1) = [];
P(1) = [];
Bnu = fittype( @(K,T,x) ((2*K*6.62617e-34*x.^3)/(299792458^2)) ./ ...
    (exp((6.62617e-34*x)./(1.38066e-23*T))-1) );
opt = fitoptions(Bnu);
opt.Lower = [-Inf -Inf];
opt.Upper = [Inf Inf];
opt.StartPoint = [100 100];
opt.DiffMinChange = 0;
opt.DiffMaxChange = 1;
opt.MaxFunEvals = 1000;
opt.MaxIter = 1000;
opt.Display = 'iter';
opt.TolFun = 1e-50;
opt.TolX = 1e-50;
fitresult = fit(n(:),P(:),Bnu,opt);
plot(n,P,'ko',linspace(min(n(:)),max(n(:)),150),Bnu(fitresult.K,fitresult.T,linspace(min(n(:)),max(n(:)),150)),'r')

