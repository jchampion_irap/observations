function [p, uncert, randparam] = MonteCarloUncert(x,ym,yp,fun,p0,log_factors,nok,plower,pupper) %#ok<*INUSL>

x = x(:); %#ok<NASGU>
ym = ym(:);
yp = yp(:);
p0 = p0(:);
plower = plower(:);
pupper = pupper(:);
log_factors = log_factors(:);

nparam = length(p0);
randparam = zeros(nparam,nok);

strcoeff = '';
for it = 1:nparam
    strcoeff = strcat(strcoeff,['parami(' num2str(it) '),']);
end
strcoeff(end) = [];

nok_trans = 0;
while nok_trans < nok;
    parami = p0.*10.^(2*(rand(nparam,1)-0.5).*log_factors);
    parami = parami(:);
    eval(['ymodel = fun(' strcoeff ',x);'])
    if fix(mean(ymodel > ym)) & fix(mean(ymodel < yp)) & fix(mean(parami > plower)) & fix(mean(parami < pupper)) %#ok<AND2>
        nok_trans = nok_trans + 1;
        randparam(:,nok_trans) = parami';
        disp([num2str(nok_trans) ' sur ' num2str(nok) '.'])
    end
end

p = mean(randparam,2);
uncert = std(randparam,0,2);
    
end