%% Cleaning workspace.
clear all
close all
clc

%% Data from our analyses.
c = 299792458; % Light velocity in vaccum in m/s.
Lines = {'[CII]';'CO(7-6)';'CO(10-9)';'CO(15-14)';'CO(17-16)';...
    'CO(19-18)';'[OI]'}; % Line names.
Instru = {'HIFI';'HIFI';'HIFI';'PACS';'PACS';'PACS';'PACS'}; % Instrument name.
lambda0 = [157.68;371.650;260.240;173.631;153.267;137.196;63.184]*1E-6;
f0 = c./lambda0;
Int = [1.29E-7;4.79E-10;1.57E-9;2E-9;2.76E-9;NaN;1.24E-6]; % Line fluxes in W/m²/sr.
dInt = [0.07E-7;0.25E-10;0.15E-9;1.82;2.96E-9;NaN;0.03E-6]; % Error of Int.

%% Beam
% PACS
Sbeam = ones(length(Int),1) * 9.4^2; % Pixel of 9.4 by 9.4 arcsec².

% HIFI
Table = [480 44.3; 640 33.2; 800 26.6; 960 22.2; 1120 19; 1410 15.2;...
    1910 11.2]; % Frequency (GHz) vs FWHM (arcsec.), from HIFI observer manual.
% Calculation of the FWHM (frequency dependent) for each HIFI observation,
% and thus the beam aperture.
for i = transpose(find(strcmpi('HIFI',Instru)))
    [~, ind] = min(abs(Table(:,1)-f0(i)*1E-9));
    FWHM = Table(ind,2) * Table(ind,1)/(f0(i)*1E-9); % Because f1/f2 = FWHM2/FWHM1.
    Sbeam(i) = pi * (FWHM/2)^2;
end

% Correction.
Sdisk = pi*(0.83/2)*(0.4/2); % Surface in arcseconds² of the Carina proplyd disk.
bff = Sdisk./Sbeam; % Beam filling factor.
Int_corr = Int./bff;
