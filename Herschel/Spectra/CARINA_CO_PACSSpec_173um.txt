# Meta data {
#  type={description="Product Type Identification", string="HPS3D"}
#  creator={description="", string="dp-pacs"}
#  creationDate={description="Creation date of this product", time="2014-03-11T16:00:46.497000Z"}
#  description={description="", string="PACS Spectral Cube, Red channel, Red channel"}
#  instrument={description="Instrument attached to this product", string="PACS"}
#  modelName={description="Model name attached to this product", string="FLIGHT"}
#  startDate={description="Start date of this product", time="2012-07-10T13:34:47.141952Z"}
#  endDate={description="End date of this product", time="2012-07-10T13:34:47.141952Z"}
#  formatVersion={description="", string="1.0"}
#  detRow={description="Number of detector rows", long=5, unit=null}
#  detCol={description="Number of detector columns", long=5, unit=null}
#  camName={description="Name of the Camera", string="Red Spectrometer"}
#  relTimeOffset={description="Relative time offset", long=0, unit=null}
#  Apid={description="", double=1157.0, unit=null}
#  subType={description="", double=4.0, unit=null}
#  compVersion={description="", double=13.0, unit=null}
#  algoNumber={description="", double=17.0, unit=null}
#  algorithm={description="", string="Spectroscopy - Least Square Fit - without glitch detection"}
#  compNumber={description="", double=16.0, unit=null}
#  compMode={description="", string="Spectroscopy Default Mode"}
#  dxid={description="", double=4001.0, unit=null}
#  spec_red_FailedSPUBuffer={description="", long=0, unit=null}
#  spec_blue_FailedSPUBuffer={description="", long=0, unit=null}
#  chopAvoidFrom={description="HSPOT: chopper avoidance angle from (deg)", double=0.0, unit=null}
#  chopAvoidTo={description="HSPOT: chopper avoidance angle to (deg)", double=0.0, unit=null}
#  chopNod={description="HSPOT: Chop/Nod observation (true for chop/nod, false for wavelength switching and unchopped)", boolean=true}
#  decoff={description="ObservationParameter", double=0.0, unit=null}
#  faintLines={description="HSPOT: faint line mode selected", boolean=true}
#  fluxUnit={description="HSPOT: units for the estimated fluxes", string="["Jy/m2"]"}
#  gratScan={description="HSPOT: Unchopped observation (true for unchopped, false for wavelength switching and chop/nod)", boolean=false}
#  lWave={description="HSPOT: line central wavelengths", string="[82.0]"}
#  lcontFlux={description="HSPOT: estimated continuum fluxes", string="[200.0]"}
#  lineFlux={description="HSPOT: estimated line fluxes", string="[1000.0]"}
#  lineStep={description="ObservationParameter", double=20.0, unit=null}
#  lineWidth={description="HSPOT: estimated line widths", string="[30.0]"}
#  lines={description="HSPOT: description of observed lines", string="[{"CO 15-14",173.63157917067412,2,10.0,30000.0,1.0,"fluxWatt","kms"},{"CO 17-16",153.26651124034612,2,10.0,30000.0,0.0,"fluxWatt","kms"},{"CO 19-18",137.19645763659602,2,10.0,30000.0,0.0,"fluxWatt","kms"}]"}
#  m={description="ObservationParameter", long=3, unit=null}
#  mapGratScanOffRep={description="ObservationParameter", long=2, unit=null}
#  mapRasterAngle={description="ObservationParameter", double=0.0, unit=null}
#  mapRasterAngleRefFrame={description="ObservationParameter", boolean=true}
#  n={description="ObservationParameter", long=3, unit=null}
#  naifid={description="HSPOT: SSO identifier", long=0, unit=null}
#  obsOverhead={description="ObservationParameter", long=180, unit=null}
#  pointStep={description="ObservationParameter", double=20.0, unit=null}
#  raoff={description="ObservationParameter", double=0.0, unit=null}
#  redshiftType={description="HSPOT: redshift type: redshift or optical", string="optical"}
#  redshiftValue={description="HSPOT: redshift", double=1.0, unit=null}
#  refSelected={description="ObservationParameter", boolean=false}
#  repeatLine={description="HSPOT: line scan repetitions", string="[1]"}
#  source={description="HSPOT: pointing mode: point: single pointing, large: mapping", string="point"}
#  throw={description="HSPOT: Chopper throw : large (6'), medium(3') or small (1')", string="small"}
#  userNODcycles={description="HSPOT: number of ABBA nod cycle repetitions", long=1, unit=null}
#  verbose={description="ObservationParameter", boolean=false}
#  widthUnit={description="HSPOT: units for the estimated line widths", string="["kms"]"}
#  level={description="Product Level", string="00"}
#  obsid={description="Observation identifier", long=1342247819, unit=null}
#  obsType={description="", long=0, unit=null}
#  obsCount={description="OBSID", long=0, unit=null}
#  camera={description="Camera", string="SPECRED"}
#  odNumber={description="Operational day number", long=1153, unit=null}
#  cusMode={description="CUS observation mode", string="PacsLineSpec"}
#  instMode={description="Instrument Mode", string="PacsLineSpec"}
#  obsMode={description="Observation mode name", string="Pointed"}
#  processingMode={description="Processing mode selected to execute the pipeline", string="BULK_REPROCESSING"}
#  origin={description="Site that created the product", string="Herschel Science Centre"}
#  slewTime={description="Scheduled start time of the slew", time="2012-07-10T13:34:29.000000Z"}
#  aorLabel={description="AOR Label as entered in HSpot", string="CO - Proplyd1"}
#  aot={description="AOT Identifier", string="Line Spectroscopy"}
#  equinox={description="Equinox of celestial coordinate system", double=2000.0, unit=null}
#  missionConfig={description="Mission configuration", string="MC_H100ASTR_P70ASTR_S66ASTR_RP"}
#  object={description="Target name", string="Carina Proplyd"}
#  raDeSys={description="Coordinate reference frame for the RA and DEC", string="ICRS"}
#  pmRA={description="Target's proper motion, RA component in arcsec/year", double=0.0, unit=″/a [1 ″/a = 1.5362818500441602E-13 rad/s]}
#  pmDEC={description="Target's proper motion, DEC component in arcsec/year", double=0.0, unit=″/a [1 ″/a = 1.5362818500441602E-13 rad/s]}
#  raNominal={description="Requested Right Ascension of pointing", double=161.63737500000002, unit=deg [1 deg = 0.017453292519943295 rad]}
#  decNominal={description="Requested Declination of pointing", double=-60.06488888888889, unit=deg [1 deg = 0.017453292519943295 rad]}
#  ra={description="Actual Right Ascension of pointing", double=161.63688680391357, unit=deg [1 deg = 0.017453292519943295 rad]}
#  dec={description="Actual Declination of pointing", double=-60.064521824322114, unit=deg [1 deg = 0.017453292519943295 rad]}
#  posAngle={description="Position Angle of pointing", double=313.20057506935, unit=deg [1 deg = 0.017453292519943295 rad]}
#  telescope={description="Name of telescope", string="Herschel Space Observatory"}
#  velocityDefinition={description="The velocity definition and frame", string="RADI-LSR"}
#  radialVelocity={description="Spacecraft velocity along the line of sight of the telescope wrt the local standard of rest: v / c = (lambda_rest - lambda) / lambda_rest", double=-26.626612376419352, unit=km/s [1 km/s = 1000.0 m/s]}
#  observer={description="Observer name", string="oberne"}
#  proposal={description="Proposal name", string="OT2_oberne_4"}
#  pointingMode={description="Pointing mode", string="Nodding"}
#  calVersion={description="Version of Calibration Tree", string="PACS_CAL_56_0"}
#  missionConfiguration={description="Mission Configuration", string="FM_0.80"}
#  instrumentConfiguration={description="Instrument Configuration", string="FM_0.80"}
#  odStartTime={description="Operational Day start time", time="2012-07-09T15:29:16.000000Z"}
#  Chopper Throw={description="", string="small"}
#  Number of Nod cycles={description="", long=1, unit=null}
#  Observing mode={description="", string="PACS Spectroscopy. Chopped. Line. Pointed."}
#  StartingTime={description="start time of the observation", string="Tue Jul 10 15:36:32 CEST 2012"}
#  Duration={description="total duration of the observation in seconds", double=2261.0, unit=s}
#  Number of Lines={description="", long=3, unit=null}
#  Line 1={description="", string="173.63157917067412 micron, 2 repetition(s), ID: CO 15-14"}
#  Line 2={description="", string="153.26651124034612 micron, 2 repetition(s), ID: CO 17-16"}
#  Line 3={description="", string="137.19645763659602 micron, 2 repetition(s), ID: CO 19-18"}
#  sliceNumber={description="Slice number", long=0, unit=null}
#  calTreeVersion={description="used version of calibration tree", long=56, unit=null}
#  rangeLow1={description="Lower wavelength end of range scanned", double=172.63102534724294, unit=µm [1 µm = 1.0E-6 m]}
#  rangeHigh1={description="High wavelength end of range scanned", double=174.67016047310656, unit=µm [1 µm = 1.0E-6 m]}
#  rangeLow2={description="Lower wavelength end of range scanned", double=57.558411652263125, unit=µm [1 µm = 1.0E-6 m]}
#  rangeHigh2={description="High wavelength end of range scanned", double=58.22798479101785, unit=µm [1 µm = 1.0E-6 m]}
#  rangeLow3={description="Lower wavelength end of range scanned", double=152.1538613354089, unit=µm [1 µm = 1.0E-6 m]}
#  rangeHigh3={description="High wavelength end of range scanned", double=154.4670152439051, unit=µm [1 µm = 1.0E-6 m]}
#  rangeLow4={description="Lower wavelength end of range scanned", double=136.0148452379362, unit=µm [1 µm = 1.0E-6 m]}
#  rangeHigh4={description="High wavelength end of range scanned", double=138.50369739853866, unit=µm [1 µm = 1.0E-6 m]}
#  rangeLow5={description="Lower wavelength end of range scanned", double=68.02178612040503, unit=µm [1 µm = 1.0E-6 m]}
#  rangeHigh5={description="High wavelength end of range scanned", double=69.25830822089593, unit=µm [1 µm = 1.0E-6 m]}
#  isInterlaced={description="", boolean=true}
#  herschelVelocityApplied={description="Herschel Velocity applied to wavelength", boolean=true}
#  aotMode={description="SLICE_INFO: observation mode description", string="Chop/Nod, pointed"}
#  rasterId={description="SLICE_INFO: raster column, raster line", string="0 0"}
#  isOffPosition={description="SLICE_INFO: off position or not", boolean=false}
#  band={description="SLICE_INFO: band name", string="R1"}
#  order={description="SLICE_INFO: order", long=1, unit=null}
#  Obcp={description="SLICE_INFO: Obcp", long=35, unit=null}
#  Filter={description="SLICE_INFO: Filter", long=0, unit=null}
#  capacitance={description="SLICE_INFO: integration capacitance", double=0.14, unit=pF [1 pF = 1.0E-12 F]}
#  pacsSliceInfoUpdated={description="PACS Slice Info keywords updated", boolean=true}
#  calBlock={description="SLICE_INFO: calibration block indicator", boolean=false}
#  lineDescription={description="SLICE_INFO: spectral line: name, wavelength, lineId", string="CO 15-14, 173.63157917067412 micron, lineID: 102"}
#  lineId={description="SLICE_INFO: line ID", long=102, unit=null}
#  minWave={description="SLICE_INFO: minimum wavelength", double=172.64603842761744, unit=null}
#  maxWave={description="SLICE_INFO: maximum wavelength", double=174.74140061381954, unit=null}
#  nodCycleNum={description="SLICE_INFO: nodding cycle number", long=1, unit=null}
#  onOffSource={description="SLICE_INFO: on/off source: 0 = not science, 1 = on-source, 2 = off-source", long=1, unit=null}
#  productNotes={description="", string="Pacs Spectrometer Pipeline Level 2 Product"}
#  onSource={description="SLICE_INFO: ", string="yes"}
#  offSource={description="SLICE_INFO: ", string="no"}
#  qflag_SLOWSAMP_p={description="", string="boolean flag: if true: more than 20% of the spectral bins have less then 6 valid data points"}
#  qflag_SLOWSAMP_p_v={description="", boolean=false}
#  oversample={description="SLICE_INFO: oversample factor", double=2.0, unit=null}
#  upsample={description="SLICE_INFO: upsample factor", double=4.0, unit=null}
#  spaxelColumn={description="PACS Spaxel column", long=2, unit=null}
#  spaxelRow={description="PACS Spaxel row", long=2, unit=null}
#  spaxelRa={description="Right Ascension of the central spaxel", double=161.6374312229026, unit=null}
#  spaxelDec={description="Declination of the central spaxel", double=-60.064870807101514, unit=null}
#  pointSourceCorrected={description="Point Source Correction Applied", boolean=true}
# } MetaData
wave_0,flux_0
Double,Double
micrometer,Jy
Wavelength,flux
172.64603842761744,3.213025954294105
172.6615897389637,3.2133439646036184
172.67714105030996,2.6739199745341327
172.6926923616562,2.674184679048925
172.70824367300253,2.9435310570201856
172.72379323250124,2.200261949090567
172.73934279199995,2.1743703488399095
172.75489235149868,2.278089273011897
172.7704419109974,1.632677189189216
172.78598970996234,2.264201777151822
172.80153750892728,1.672641835998184
172.81708530789223,1.7814360960523274
172.83263310685714,1.8618224236984726
172.8481791365943,1.7539539105944137
172.8637251663315,2.248234191775704
172.87927119606866,2.352455716180412
172.89481722580584,2.4322438254282144
172.91036147761355,2.904579398953058
172.9259057294213,2.6459992033178104
172.94144998122903,2.8369548519785153
172.95699423303677,2.7691501690003357
172.97253669820563,2.581661176939129
172.9880791633745,2.8678059075780022
173.00362162854339,2.7242644264553832
173.01916409371225,2.376068847634138
173.03470476352516,2.6986555407429904
173.05024543333806,2.403552456814779
173.06578610315097,2.4234477595041315
173.08132677296385,3.1020720440221408
173.09686563869593,2.7074512096869348
173.11240450442804,2.682767169037801
173.12794337016015,2.7038928006525036
173.14348223589224,2.4388819244207016
173.159019288811,2.297889987098092
173.17455634172978,2.562641194265421
173.19009339464856,2.38186075319949
173.20563044756733,2.336589721745488
173.22116567893255,2.5212804496451144
173.23670091029777,2.512206100068445
173.25223614166296,2.8078607913306093
173.26777137302818,2.964242039870252
173.2833047740919,2.727392864381809
173.29883817515565,2.596670750080152
173.3143715762194,2.6505211554974495
173.32990497728315,2.425407325308384
173.34543653928978,2.621432032653102
173.36096810129646,2.8555030954113017
173.3764996633031,2.834768420302994
173.39203122530978,2.762629102338643
173.40756093949605,2.781161734614591
173.42309065368232,2.5810365851360806
173.43862036786862,2.405848795077689
173.45415008205492,2.34434371888822
173.46967793964984,2.4175362409703007
173.48520579724476,2.5332298750354147
173.5007336548397,2.316653961749301
173.51626151243468,2.4491878548093986
173.53178750465963,2.3878122963172843
173.54731349688458,2.2322621098163706
173.56283948910954,2.427945907819035
173.57836548133452,2.457313824118597
173.59388959940318,2.432562856653272
173.60941371747185,2.697199983226066
173.62493783554052,3.010603463204112
173.6404619536092,3.1098943484551707
173.6559841887276,3.0908534195310526
173.67150642384598,2.964766296448262
173.68702865896438,2.6333636567464556
173.70255089408278,2.3653887351499305
173.7180712374493,2.620179282964457
173.73359158081578,2.5957997021815222
173.74911192418227,2.6731389415907096
173.7646322675488,2.662775649290547
173.78015071035412,2.473333764159494
173.79566915315945,2.2367640322567857
173.81118759596475,2.3835243173038694
173.82670603877006,2.539576464550353
173.84222257219727,2.496827351202888
173.8577391056245,2.5452348854538602
173.87325563905168,2.4907099142797637
173.88877217247887,2.3961822826840358
173.90428678770337,2.4051282669210168
173.91980140292787,2.751047958639944
173.93531601815238,2.538369456466671
173.9508306333769,2.481144873039049
173.9663433215665,2.285470841743408
173.98185600975611,1.988729225125787
173.99736869794572,2.1929355716203123
174.0128813861353,2.3547187039661903
174.02839213845016,2.5722368676416814
174.04390289076503,2.426318766260009
174.05941364307986,2.347396925396648
174.0749243953947,2.0314777765983707
174.0904332029873,1.88531132483243
174.10594201057995,2.1528414253608146
174.12145081817255,2.062789698374762
174.13695962576514,2.2144049085612463
174.15246647978046,2.232441336160827
174.16797333379577,2.2544865454189678
174.18348018781106,2.0496937257231407
174.1989870418263,2.5152607033543855
174.21449193340158,2.5256089649753344
174.2299968249768,2.350748715330062
174.24550171655204,2.469121547543906
174.26100660812727,2.383037294471766
174.27650952839215,2.6036074982699198
174.29201244865703,2.295439449555991
174.30751536892188,2.2759969280028645
174.3230182891868,1.9911005578360867
174.33851922926334,1.9463895839449148
174.3540201693399,2.5127406014079785
174.36952110941647,3.116832663050799
174.38502204949305,3.251603293050391
174.40052100049576,3.6029401230028695
174.4160199514985,3.5945933620313277
174.43151890250124,2.6836662163808382
174.44701785350395,2.7874962416022466
174.4625148065397,2.0134306349982696
174.47801175957545,2.469078588131661
174.4935087126112,2.772143196704023
174.50900566564695,2.697977464284841
174.52450061181503,3.0542694996669177
174.53999555798308,2.1986075685303046
174.55549050415112,2.190632253622076
174.5709854503192,2.2882280221588873
174.58647838071124,2.288455431641766
174.60197131110328,2.3566177640972152
174.61746424149533,2.356852016874688
174.6329571718874,0.5475595366981695
174.64844909216342,0.5476139725443565
174.66394101243944,0.5476684192151416
174.67943293271546,0.547722876713754
174.69492485299148,0.5477773450434239
174.7104167732675,0.5478318242073829
174.72590869354352,0.5478863142088639
174.74140061381954,0.547940815051101
