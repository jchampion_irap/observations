# Meta data {
#  type={description="Product Type Identification", string="HPS3D"}
#  creator={description="", string="dp-pacs"}
#  creationDate={description="Creation date of this product", time="2014-03-26T13:51:43.056000Z"}
#  description={description="", string="PACS Spectral Cube, Red channel, Red channel"}
#  instrument={description="Instrument attached to this product", string="PACS"}
#  modelName={description="Model name attached to this product", string="FLIGHT"}
#  startDate={description="Start date of this product", time="2012-09-11T16:06:39.891000Z"}
#  endDate={description="End date of this product", time="2012-09-11T16:12:54.766000Z"}
#  formatVersion={description="", string="1.0"}
#  detRow={description="Number of detector rows", long=5, unit=null}
#  detCol={description="Number of detector columns", long=5, unit=null}
#  camName={description="Name of the Camera", string="Red Spectrometer"}
#  relTimeOffset={description="Relative time offset", long=0, unit=null}
#  Apid={description="", double=1157.0, unit=null}
#  subType={description="", double=4.0, unit=null}
#  compVersion={description="", double=13.0, unit=null}
#  algoNumber={description="", double=17.0, unit=null}
#  algorithm={description="", string="Spectroscopy - Least Square Fit - without glitch detection"}
#  compNumber={description="", double=16.0, unit=null}
#  compMode={description="", string="Spectroscopy Default Mode"}
#  dxid={description="", double=4001.0, unit=null}
#  spec_red_FailedSPUBuffer={description="", long=0, unit=null}
#  spec_blue_FailedSPUBuffer={description="", long=0, unit=null}
#  chopAvoidFrom={description="ObservationParameter", double=0.0, unit=null}
#  chopAvoidTo={description="ObservationParameter", double=0.0, unit=null}
#  chopNod={description="HSPOT: Chop/Nod observation (true for chop/nod, false for wavelength switching and unchopped)", boolean=false}
#  decoff={description="ObservationParameter", double=-5.379, unit=null}
#  faintLines={description="HSPOT: faint line mode selected", boolean=true}
#  fluxUnit={description="HSPOT: units for the estimated fluxes", string="["Jy/m2"]"}
#  gratScan={description="HSPOT: Unchopped observation (true for unchopped, false for wavelength switching and chop/nod)", boolean=true}
#  lWave={description="HSPOT: line central wavelengths", string="[82.0]"}
#  lcontFlux={description="HSPOT: estimated continuum fluxes", string="[200.0]"}
#  lineFlux={description="HSPOT: estimated line fluxes", string="[1000.0]"}
#  lineStep={description="ObservationParameter", double=20.0, unit=null}
#  lineWidth={description="HSPOT: estimated line widths", string="[30.0]"}
#  lines={description="HSPOT: description of observed lines", string="[{"CO 15-14",173.63157917067412,2,10.0,30000.0,1.0,"fluxWatt","kms"},{"CO 17-16",153.26651124034612,2,10.0,30000.0,0.0,"fluxWatt","kms"},{"CO 19-18",137.19645763659602,2,10.0,30000.0,0.0,"fluxWatt","kms"}]"}
#  m={description="ObservationParameter", long=3, unit=null}
#  mapGratScanOffRep={description="ObservationParameter", long=2, unit=null}
#  mapRasterAngle={description="ObservationParameter", double=0.0, unit=null}
#  mapRasterAngleRefFrame={description="ObservationParameter", boolean=true}
#  n={description="ObservationParameter", long=3, unit=null}
#  naifid={description="HSPOT: SSO identifier", long=0, unit=null}
#  obsOverhead={description="ObservationParameter", long=180, unit=null}
#  pointStep={description="ObservationParameter", double=20.0, unit=null}
#  pointedRefDec={description="ObservationParameter", double=-5.379, unit=null}
#  pointedRefOffset={description="ObservationParameter", boolean=false}
#  pointedRefRa={description="ObservationParameter", double=83.544, unit=null}
#  raoff={description="ObservationParameter", double=83.544, unit=null}
#  redshiftType={description="HSPOT: redshift type: redshift or optical", string="optical"}
#  redshiftValue={description="HSPOT: redshift", double=1.0, unit=null}
#  refSelected={description="ObservationParameter", boolean=true}
#  repeatLine={description="HSPOT: line scan repetitions", string="[1]"}
#  source={description="HSPOT: pointing mode: point: single pointing, large: mapping", string="point"}
#  throw={description="ObservationParameter", string="small"}
#  userNODcycles={description="HSPOT: number of ABBA nod cycle repetitions", long=1, unit=null}
#  verbose={description="ObservationParameter", boolean=false}
#  widthUnit={description="HSPOT: units for the estimated line widths", string="["kms"]"}
#  level={description="Product Level", string="00"}
#  obsid={description="Observation identifier", long=1342250899, unit=null}
#  obsType={description="", long=0, unit=null}
#  obsCount={description="OBSID", long=0, unit=null}
#  camera={description="Camera", string="SPECRED"}
#  odNumber={description="Operational day number", long=1216, unit=null}
#  cusMode={description="CUS observation mode", string="PacsLineSpec"}
#  instMode={description="Instrument Mode", string="PacsLineSpec"}
#  obsMode={description="Observation mode name", string="Pointed"}
#  processingMode={description="Processing mode selected to execute the pipeline", string="BULK_REPROCESSING"}
#  origin={description="Site that created the product", string="Herschel Science Centre"}
#  slewTime={description="Scheduled start time of the slew", time="2012-09-11T16:00:01.000000Z"}
#  aorLabel={description="AOR Label as entered in HSpot", string="CO - 244-440"}
#  aot={description="AOT Identifier", string="Line Spectroscopy"}
#  equinox={description="Equinox of celestial coordinate system", double=2000.0, unit=null}
#  missionConfig={description="Mission configuration", string="MC_H102ASTR_P70ASTR_S66ASTR_RP"}
#  object={description="Target name", string="244-440"}
#  raDeSys={description="Coordinate reference frame for the RA and DEC", string="ICRS"}
#  pmRA={description="Target's proper motion, RA component in arcsec/year", double=0.0, unit=″/a [1 ″/a = 1.5362818500441602E-13 rad/s]}
#  pmDEC={description="Target's proper motion, DEC component in arcsec/year", double=0.0, unit=″/a [1 ″/a = 1.5362818500441602E-13 rad/s]}
#  raNominal={description="Requested Right Ascension of pointing", double=83.85158333333334, unit=deg [1 deg = 0.017453292519943295 rad]}
#  decNominal={description="Requested Declination of pointing", double=-5.410694444444445, unit=deg [1 deg = 0.017453292519943295 rad]}
#  ra={description="Actual Right Ascension of pointing", double=83.75139781575558, unit=deg [1 deg = 0.017453292519943295 rad]}
#  dec={description="Actual Declination of pointing", double=-5.400300182157064, unit=deg [1 deg = 0.017453292519943295 rad]}
#  posAngle={description="Position Angle of pointing", double=85.41755371478804, unit=deg [1 deg = 0.017453292519943295 rad]}
#  telescope={description="Name of telescope", string="Herschel Space Observatory"}
#  velocityDefinition={description="The velocity definition and frame", string="RADI-LSR"}
#  radialVelocity={description="Spacecraft velocity along the line of sight of the telescope wrt the local standard of rest: v / c = (lambda_rest - lambda) / lambda_rest", double=8.187599822131503, unit=km/s [1 km/s = 1000.0 m/s]}
#  observer={description="Observer name", string="oberne"}
#  proposal={description="Proposal name", string="OT2_oberne_4"}
#  pointingMode={description="Pointing mode", string="Nodding"}
#  calVersion={description="Version of Calibration Tree", string="PACS_CAL_56_0"}
#  missionConfiguration={description="Mission Configuration", string="FM_0.80"}
#  instrumentConfiguration={description="Instrument Configuration", string="FM_0.80"}
#  odStartTime={description="Operational Day start time", time="2012-09-10T16:46:56.000000Z"}
#  Observing mode={description="", string="PACS Spectroscopy. Unchopped. Line. Pointed."}
#  StartingTime={description="start time of the observation", string="Tue Sep 11 18:02:04 CEST 2012"}
#  Duration={description="total duration of the observation in seconds", double=1787.0, unit=s}
#  Number of Lines={description="", long=3, unit=null}
#  Line 1={description="", string="173.63157917067412 micron, 2 repetition(s), ID: CO 15-14"}
#  Line 2={description="", string="153.26651124034612 micron, 2 repetition(s), ID: CO 17-16"}
#  Line 3={description="", string="137.19645763659602 micron, 2 repetition(s), ID: CO 19-18"}
#  sliceNumber={description="Slice number", long=1, unit=null}
#  calTreeVersion={description="used version of calibration tree", long=56, unit=null}
#  rangeLow1={description="Lower wavelength end of range scanned", double=172.1948991614291, unit=µm [1 µm = 1.0E-6 m]}
#  rangeHigh1={description="High wavelength end of range scanned", double=175.1383954356052, unit=µm [1 µm = 1.0E-6 m]}
#  rangeLow2={description="Lower wavelength end of range scanned", double=57.413082310479474, unit=µm [1 µm = 1.0E-6 m]}
#  rangeHigh2={description="High wavelength end of range scanned", double=58.383979002961304, unit=µm [1 µm = 1.0E-6 m]}
#  rangeLow3={description="Lower wavelength end of range scanned", double=151.6586068599568, unit=µm [1 µm = 1.0E-6 m]}
#  rangeHigh3={description="High wavelength end of range scanned", double=154.99052459278346, unit=µm [1 µm = 1.0E-6 m]}
#  rangeLow4={description="Lower wavelength end of range scanned", double=135.48158702387957, unit=µm [1 µm = 1.0E-6 m]}
#  rangeHigh4={description="High wavelength end of range scanned", double=139.06266352105882, unit=µm [1 µm = 1.0E-6 m]}
#  rangeLow5={description="Lower wavelength end of range scanned", double=67.75525304863248, unit=µm [1 µm = 1.0E-6 m]}
#  rangeHigh5={description="High wavelength end of range scanned", double=69.53759403663457, unit=µm [1 µm = 1.0E-6 m]}
#  isInterlaced={description="", boolean=false}
#  herschelVelocityApplied={description="Herschel Velocity applied to wavelength", boolean=true}
#  aotMode={description="SLICE_INFO: observation mode description", string="Unchopped"}
#  rasterId={description="SLICE_INFO: raster column, raster line", string="0 0"}
#  isOffPosition={description="SLICE_INFO: off position or not", boolean=false}
#  band={description="SLICE_INFO: band name", string="R1"}
#  order={description="SLICE_INFO: order", long=1, unit=null}
#  Filter={description="SLICE_INFO: Filter", long=0, unit=null}
#  capacitance={description="SLICE_INFO: integration capacitance", double=0.14, unit=pF [1 pF = 1.0E-12 F]}
#  pacsSliceInfoUpdated={description="PACS Slice Info keywords updated", boolean=true}
#  calBlock={description="SLICE_INFO: calibration block indicator", boolean=false}
#  lineDescription={description="SLICE_INFO: spectral line: name, wavelength, lineId", string="CO 17-16, 153.26651124034612 micron, lineID: 103"}
#  lineId={description="SLICE_INFO: line ID", long=103, unit=null}
#  minWave={description="SLICE_INFO: minimum wavelength", double=151.67837789035974, unit=null}
#  maxWave={description="SLICE_INFO: maximum wavelength", double=155.05735858606346, unit=null}
#  nodCycleNum={description="SLICE_INFO: nodding cycle number", long=1, unit=null}
#  onOffSource={description="SLICE_INFO: on/off source: 0 = not science, 1 = on-source, 2 = off-source", long=1, unit=null}
#  Obcp={description="SLICE_INFO: Obcp", long=28, unit=null}
#  productNotes={description="", string="Pacs Spectrometer Pipeline Level 2 Product"}
#  onSource={description="SLICE_INFO: ", string="yes"}
#  offSource={description="SLICE_INFO: ", string="no"}
#  qflag_SLOWSAMP_p={description="", string="boolean flag: if true: more than 20% of the spectral bins have less then 6 valid data points"}
#  qflag_SLOWSAMP_p_v={description="", boolean=false}
#  oversample={description="SLICE_INFO: oversample factor", double=2.0, unit=null}
#  upsample={description="SLICE_INFO: upsample factor", double=4.0, unit=null}
#  spaxelColumn={description="PACS Spaxel column", long=2, unit=null}
#  spaxelRow={description="PACS Spaxel row", long=2, unit=null}
#  spaxelRa={description="Right Ascension of the central spaxel", double=83.85115818817741, unit=null}
#  spaxelDec={description="Declination of the central spaxel", double=-5.410628084933687, unit=null}
#  pointSourceCorrected={description="Point Source Correction Applied", boolean=false}
# } MetaData
wave_0,flux_0
Double,Double
micrometer,Jy
Wavelength,flux
151.67837789035974,238.27966299565432
151.69407839292185,238.27966299565432
151.70977889548396,240.43452275509216
151.7254793980461,240.43452275509216
151.74117990060822,242.73735636986186
151.7568811033915,242.5647968594303
151.77258230617477,242.37424393492228
151.78828350895805,242.7502822721554
151.80398471174132,242.62374217053514
151.81968660875822,242.35048802841183
151.83538850577514,242.7718069871298
151.85109040279207,242.17994177408406
151.866792299809,242.55953201273732
151.88249488506398,242.51869121187994
151.89819747031896,242.48637359373842
151.91390005557395,242.82633156043428
151.92960264082888,242.6426787754713
151.9453059083183,242.72819484843055
151.9610091758077,242.39188439968464
151.9767124432971,242.2608035904145
151.9924157107865,241.86495142920347
152.0081196544986,241.9323793128781
152.02382359821073,241.59234480164102
152.03952754192284,241.32393305567126
152.05523148563498,241.11600209320505
152.07093609955004,240.58223004557598
152.0866407134651,240.84919040886766
152.10234532738016,240.795745368254
152.1180499412952,240.68261678057124
152.13375521938536,240.8784794765815
152.1494604974755,240.98516971814124
152.16516577556564,240.8947162741518
152.18087105365578,240.91779833566395
152.1965769898851,240.6786083538675
152.21228292611443,240.1290838101785
152.22798886234375,239.78018537965147
152.24369479857307,239.60238976587385
152.2594013868976,239.58795043820464
152.27510797522206,239.71684110117337
152.29081456354658,239.9150318080205
152.30652115187104,239.80379221873034
152.32222838623866,239.7895839199352
152.3379356206063,239.55065345813466
152.35364285497388,239.47848840231018
152.36935008934148,239.5149116803334
152.38505796369202,239.37013982354767
152.40076583804262,239.45541851224743
152.41647371239316,239.51642403813386
152.4321815867437,239.44010176426656
152.447890095009,239.63464018626047
152.4635986032743,239.53854432005286
152.47930711153958,239.5888856873402
152.49501561980486,239.70171736368107
152.51072475590854,239.53594551977898
152.52643389201225,239.7513093232112
152.54214302811596,239.47787225875763
152.55785216421964,239.4931826011334
152.57356192207735,239.59473074756477
152.58927167993505,239.4884752179496
152.6049814377928,239.63091618528378
152.6206911956505,239.55073160507607
152.63640156916972,239.46398622861176
152.65211194268898,239.36853965952815
152.66782231620823,239.03611543576721
152.68353268972749,238.8840402679877
152.69924367280765,238.5741392294893
152.71495465588788,238.56778046417838
152.73066563896805,238.44470855824076
152.7463766220483,238.49549493673285
152.7620882085808,238.7157722634877
152.7777997951133,238.75123886394047
152.79351138164583,239.1165756258564
152.80922296817835,239.01999500596088
152.8249351520464,238.85644001352495
152.84064733591447,238.79985861677855
152.85635951978253,238.60324741472098
152.8720717036506,238.8171013475946
152.88778447872937,238.8449881823027
152.90349725380815,239.17484085187692
152.9192100288869,239.31624504882456
152.93492280396572,239.306089589015
152.95063616412224,239.3570900682061
152.96634952427877,239.17788288190636
152.98206288443532,239.3286530724131
152.99777624459188,239.36468578856557
153.01349018368515,239.47987438841545
153.02920412277842,239.45743704100573
153.0449180618717,239.19496888804363
153.06063200096497,239.18886474407537
153.07634651284584,239.3759735842184
153.09206102472672,239.65657817867253
153.1077755366076,240.04497915234296
153.12349004848846,240.45078979038104
153.13920512699974,240.76169044135645
153.15492020551096,241.84954111596744
153.17063528402218,244.21379747369875
153.18635036253346,249.9485886767807
153.20206600150973,257.53902664655567
153.217781640486,271.61478884271287
153.23349727946228,283.61670028423885
153.24921291843856,302.3397358105824
153.2649291117064,310.72406949386726
153.2806453049743,320.33380306735125
153.29636149824216,318.45783145305097
153.31207769151,310.48412253622325
153.32779443288797,299.20163642756495
153.34351117426587,280.9581624741933
153.35922791564383,269.2147291604336
153.3749446570217,254.66777541497555
153.39066194032003,248.40855866535233
153.40637922361836,242.34480339816116
153.4220965069167,240.8129230045067
153.43781379021496,239.54354437204987
153.45353160923594,239.19801608955277
153.4692494282569,238.37811665115714
153.48496724727784,237.98567225616776
153.5006850662988,237.1402503553061
153.5164034148365,236.9429816822425
153.53212176337422,236.67188014890215
153.54784011191197,236.59575754613965
153.56355846044966,236.3929621608806
153.5792773322902,236.2240444513768
153.59499620413072,235.79467905226252
153.61071507597123,235.57975230008964
153.62643394781173,235.23188857837835
153.64215333673295,235.1540075940461
153.65787272565416,234.99469045876987
153.67359211457537,234.80498478198066
153.68931150349658,234.55617229555628
153.70503140326832,234.4017316320877
153.72075130304,234.34666752187414
153.73647120281169,234.42610285653652
153.75219110258342,234.4920015221711
153.76791150696732,234.59149415116892
153.78363191135116,234.54143483135084
153.79935231573506,234.53605964533122
153.8150727201189,234.31964485504213
153.83079362286853,234.3622996013748
153.84651452561815,234.11162651139435
153.86223542836774,234.18773048678264
153.87795633111733,233.91683406862504
153.89367772597808,233.84119450684372
153.90939912083883,233.64230253222604
153.9251205156996,233.47812676178913
153.94084191056038,233.59521438658302
153.95656379126962,233.46216182489314
153.97228567197885,233.52625723654342
153.9880075526881,233.424095057954
154.00372943339735,233.45788485294614
154.01945179368425,233.3833961569929
154.03517415397118,233.32860693513823
154.0508965142581,233.25678118988722
154.06661887454504,233.241923075924
154.08234170813074,233.16700119940904
154.09806454171638,233.09265885309557
154.11378737530208,233.14352379879006
154.12951020888775,233.18071649032618
154.14523350948514,233.08061976305794
154.1609568100825,233.20847017086132
154.17668011067985,233.16773906813197
154.19240341127724,232.9982405810265
154.20812717259113,233.1473822898799
154.22385093390503,233.00295991281178
154.23957469521892,233.00568132857092
154.25529845653278,232.82307397781565
154.2710226722599,232.7568634149206
154.28674688798702,232.42076633206983
154.3024711037141,232.2156129915063
154.3181953194412,231.8779938590914
154.33391998327005,231.64847047488294
154.34964464709896,231.6547071363212
154.3653693109278,231.5990175504064
154.38109397475665,231.71314843731102
154.39681908036775,231.70310978727426
154.41254418597885,231.83361939425674
154.4282692915899,231.55786071148563
154.44399439720098,231.74153546992494
154.45971993826655,231.75359654503131
154.4754454793321,231.65413561113414
154.4911710203977,231.85766583203483
154.5068965614633,231.6017673364155
154.52262253164753,231.29050395648517
154.53834850183176,231.09436298393857
154.55407447201603,230.66657778990358
154.56980044220026,230.76272187142607
154.58552683515921,230.95820064095832
154.60125322811817,230.83071629094155
154.61697962107712,231.16318989817444
154.63270601403605,231.1107926783748
154.64843282341758,231.0396114833556
154.6641596327991,230.98909720375684
154.67988644218065,231.00552967792714
154.69561325156218,230.984148993859
154.71134047100608,230.7315686322986
154.72706769044996,230.951106225965
154.74279490989383,230.95802506146003
154.75852212933773,230.9015244060256
154.7742497524756,231.0243446498264
154.78997737561343,231.17334427858083
154.8057049987513,231.41809341216896
154.82143262188913,231.74131938595286
154.83716064234443,231.97658002984167
154.85288866279973,232.47745993613586
154.86861668325503,232.6201715181343
154.88434470371033,232.08918603110826
154.9000731150984,232.3587385607484
154.91580152648646,231.54701329962543
154.93152993787453,228.64141194021317
154.9472583492626,229.7295861427196
154.96298695451986,229.7295861427196
154.97871555977713,229.7295861427196
154.9944441650344,229.7295861427196
155.01017277029166,229.7295861427196
155.02590137554893,229.7295861427196
155.0416299808062,229.7295861427196
155.05735858606346,229.7295861427196
