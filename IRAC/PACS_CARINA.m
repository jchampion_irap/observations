%% Cleaning workspace
clear all
close all
clc

%% 70 um. (Jy/pix)
S = [2.77458 8.12178]; % Sum.
dS = [1.66571 2.84987]; % Error on the sum.
n = [25 81]; % Number of pixels.
m = S./n; % Mean.
dm = dS./n; % Error of the mean.
% dev = [0.000322696 0.000425922]; % Standard deviation.
ma = [0.141701 0.141701]; % Maximum.

pixsr = ((2/3600)*pi/180)^2; % 1pix = 2as, conversion to sr here.

% Mean Background emission.
mf = (S(2)-S(1))/(n(2)-n(1));
dmf = sqrt((dS(1)^2+dS(2)^2))/(n(2)-n(1)); %dmf = (dS(1)+dS(2))/(n(2)-n(1));
% Conversion
mf_MJypersr = 1e-6*mf/pixsr;
dmf_MJypersr = 1e-6*dmf/pixsr;

% Mean CARINA emission * bff (in area n°1).
bff_times_mc = n(2)*(m(1)-m(2))/(n(2)-n(1));
dbff_times_mc = (n(2)/(n(2)-n(1)))*sqrt(dm(1)^2+dm(2)^2); %dbff_times_mc = n(2)*(dm(1)+dm(2))/(n(2)-n(1));
% Conversion
bff_times_mc_MJypersr = 1e-6*bff_times_mc/pixsr;
dbff_times_mc_MJypersr = 1e-6*dbff_times_mc/pixsr;

% Max CARINA emission.
mac = ma(1)-mf;
dmac = dmf;
% Conversion
mac_MJypersr = 1e-6*mac/pixsr;
dmac_MJypersr = 1e-6*dmac/pixsr;

disp([char(10) 'PACS 70um' char(10) '========='])
disp(['Background : ' num2str(mf_MJypersr) ' ± ' num2str(dmf_MJypersr) ' MJy/sr.'])
disp(['CARINA : mean (*bff) = ' num2str(bff_times_mc_MJypersr) ' ± ' num2str(dbff_times_mc_MJypersr) ' MJy/sr.']) 
disp(['CARINA : max = ' num2str(mac_MJypersr) ' ± ' num2str(dmac_MJypersr) ' MJy/sr.']) 

%% 160 um. (Jy/pix)
S = [1.06429 4.2543]; % Sum.
dS = [1.03164 2.0626]; % Error on the sum.
n = [9 42]; % Number of pixels.
m = S./n; % Mean.
dm = dS./n; % Error of the mean.
% dev = [0.00011962 0.000284684]; % Standard deviation.
ma = [0.13127 0.13127]; % Maximum.

pixsr = ((3/3600)*pi/180)^2; % 1pix = 3as, conversion to sr here.

% Mean Background emission.
mf = (S(2)-S(1))/(n(2)-n(1));
dmf = sqrt((dS(1)^2+dS(2)^2))/(n(2)-n(1)); %dmf = (dS(1)+dS(2))/(n(2)-n(1));
% Conversion
mf_MJypersr = 1e-6*mf/pixsr;
dmf_MJypersr = 1e-6*dmf/pixsr;

% Mean CARINA emission * bff (in area n°1).
bff_times_mc = n(2)*(m(1)-m(2))/(n(2)-n(1));
dbff_times_mc = (n(2)/(n(2)-n(1)))*sqrt(dm(1)^2+dm(2)^2); %dbff_times_mc = n(2)*(dm(1)+dm(2))/(n(2)-n(1));
% Conversion
bff_times_mc_MJypersr = 1e-6*bff_times_mc/pixsr;
dbff_times_mc_MJypersr = 1e-6*dbff_times_mc/pixsr;

% Max CARINA emission.
mac = ma(1)-mf;
dmac = dmf;
% Conversion
mac_MJypersr = 1e-6*mac/pixsr;
dmac_MJypersr = 1e-6*dmac/pixsr;

disp([char(10) 'PACS 160um' char(10) '=========='])
disp(['Background : ' num2str(mf_MJypersr) ' ± ' num2str(dmf_MJypersr) ' MJy/sr.'])
disp(['CARINA : mean (*bff) = ' num2str(bff_times_mc_MJypersr) ' ± ' num2str(dbff_times_mc_MJypersr) ' MJy/sr.']) 
disp(['CARINA : max = ' num2str(mac_MJypersr) ' ± ' num2str(dmac_MJypersr) ' MJy/sr.']) 