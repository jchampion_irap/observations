%% Cleaning workspace
clear all
close all
clc

%% 8.0 um.
S = [13607.7 42678.5]; % Sum.
dS = [116.652 206.588]; % Error on the sum.
n = [247 980]; % Number of pixels.
m = S./n; % Mean.
dm = dS./n; % Error of the mean.
% dev = [18.3605 12.0858]; % Standard deviation.
ma = [123.638 123.638]; % Maximum.

% Mean Background emission.
mf = (S(2)-S(1))/(n(2)-n(1));
dmf = sqrt((dS(1)^2+dS(2)^2))/(n(2)-n(1)); %dmf = (dS(1)+dS(2))/(n(2)-n(1));

% Mean CARINA emission * bff (in area n°1).
bff_times_mc = n(2)*(m(1)-m(2))/(n(2)-n(1));
dbff_times_mc = (n(2)/(n(2)-n(1)))*sqrt(dm(1)^2+dm(2)^2); %dbff_times_mc = n(2)*(dm(1)+dm(2))/(n(2)-n(1));

% Max CARINA emission.
mac = ma(1)-mf;
dmac = dmf;

disp([char(10) 'IRAC 8.0um' char(10) '=========='])
disp(['Background : ' num2str(mf) ' ± ' num2str(dmf) ' MJy/sr.'])
disp(['CARINA : mean (*bff) = ' num2str(bff_times_mc) ' ± ' num2str(dbff_times_mc) ' MJy/sr.']) 
disp(['CARINA : max = ' num2str(mac) ' ± ' num2str(dmac) ' MJy/sr.']) 

%% 5.8 um.
S = [4350.65 12198.2]; % Sum.
dS = [65.9595 110.445]; % Error on the sum.
n = [247 979]; % Number of pixels.
m = S./n; % Mean.
dm = dS./n; % Error of the mean.
% dev = [9.38124 5.69599]; % Standard deviation.
ma = [53.9206 53.9206]; % Maximum.

% Mean Background emission.
mf = (S(2)-S(1))/(n(2)-n(1));
dmf = sqrt((dS(1)^2+dS(2)^2))/(n(2)-n(1)); %dmf = (dS(1)+dS(2))/(n(2)-n(1));

% Mean CARINA emission * bff (in area n°1).
bff_times_mc = n(2)*(m(1)-m(2))/(n(2)-n(1));
dbff_times_mc = (n(2)/(n(2)-n(1)))*sqrt(dm(1)^2+dm(2)^2); %dbff_times_mc = n(2)*(dm(1)+dm(2))/(n(2)-n(1));

% Max CARINA emission.
mac = ma(1)-mf;
dmac = dmf;

disp([char(10) 'IRAC 5.8um' char(10) '=========='])
disp(['Background : ' num2str(mf) ' ± ' num2str(dmf) ' MJy/sr.'])
disp(['CARINA : mean (*bff) = ' num2str(bff_times_mc) ' ± ' num2str(dbff_times_mc) ' MJy/sr.']) 
disp(['CARINA : max = ' num2str(mac) ' ± ' num2str(dmac) ' MJy/sr.'])

%% 4.5 um.
S = [1373.47 4204.21]; % Sum.
dS = [37.0604 64.8399]; % Error on the sum.
n = [247 979]; % Number of pixels.
m = S./n; % Mean.
dm = dS./n; % Error of the mean.
% dev = [3.15709 1.79867]; % Standard deviation.
ma = [25.4527 25.4527]; % Maximum.

% Mean Background emission.
mf = (S(2)-S(1))/(n(2)-n(1));
dmf = sqrt((dS(1)^2+dS(2)^2))/(n(2)-n(1)); %dmf = (dS(1)+dS(2))/(n(2)-n(1));

% Mean CARINA emission * bff (in area n°1).
bff_times_mc = n(2)*(m(1)-m(2))/(n(2)-n(1));
dbff_times_mc = (n(2)/(n(2)-n(1)))*sqrt(dm(1)^2+dm(2)^2); %dbff_times_mc = n(2)*(dm(1)+dm(2))/(n(2)-n(1));

% Max CARINA emission.
mac = ma(1)-mf;
dmac = dmf;

disp([char(10) 'IRAC 4.5um' char(10) '=========='])
disp(['Background : ' num2str(mf) ' ± ' num2str(dmf) ' MJy/sr.'])
disp(['CARINA : mean (*bff) = ' num2str(bff_times_mc) ' ± ' num2str(dbff_times_mc) ' MJy/sr.']) 
disp(['CARINA : max = ' num2str(mac) ' ± ' num2str(dmac) ' MJy/sr.'])

%% 3.6 um.
S = [1232.15 3632.97]; % Sum.
dS = [35.1063 60.2741]; % Error on the sum.
n = [247 979]; % Number of pixels.
m = S./n; % Mean.
dm = dS./n; % Error of the mean.
% dev = [3.15709 2.63048]; % Standard deviation.
ma = [16.1971 16.1971]; % Maximum.

% Mean Background emission.
mf = (S(2)-S(1))/(n(2)-n(1));
dmf = (dS(1)+dS(2))/(n(2)-n(1));

% Mean CARINA emission * bff (in area n°1).
bff_times_mc = n(2)*(m(1)-m(2))/(n(2)-n(1));
dbff_times_mc = n(2)*(dm(1)+dm(2))/(n(2)-n(1));

% Max CARINA emission.
mac = ma(1)-mf;
dmac = dmf;

disp([char(10) 'IRAC 3.6um' char(10) '=========='])
disp(['Background : ' num2str(mf) ' ± ' num2str(dmf) ' MJy/sr.'])
disp(['CARINA : mean (*bff) = ' num2str(bff_times_mc) ' ± ' num2str(dbff_times_mc) ' MJy/sr.']) 
disp(['CARINA : max = ' num2str(mac) ' ± ' num2str(dmac) ' MJy/sr.'])