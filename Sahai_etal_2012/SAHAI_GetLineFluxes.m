function SAHAI_LineFluxes = SAHAI_GetLineFluxes(update)

if update == 1
    % Create data file with columns as follow :
    % Instrument | Line | Wavelength (um) | FWHM (") | Object | Type of value | Value | Uncertainty
    nSAHAI_LineFluxes = 7;
    SAHAI_LineFluxes = cell(nSAHAI_LineFluxes+1,8); 
    SAHAI_LineFluxes(1,:) = {'Instrument','Line','Wavelength (um)','FWHM (")','Object','Type of Value','Value (W/m²/sr)','Uncertainty (%)'};
    save('SAHAI_LineFluxes.mat','SAHAI_LineFluxes')
    
    % Data from Sahai et al. (2012)
    % Sahai R., Güsten R. and Morris M. R. (2012), Are large, cometary-shaped
    % proplyds really evaporating gas globules?. ApJL (761:L21).
    Line = {'CO 3-2';'CO 4-3';'CO 6-5';'CO 7-6';'13CO 3-2';...
        'HCO+ 4-3';'HCN 4-3'}; % Line names.
    Instru = {'APEX-FLASH+';'APEX-FLASH+';'APEX-CHAMP+';'APEX-CHAMP+';'APEX-FLASH+';'APEX-FLASH+';'APEX-FLASH+';'APEX-FLASH+'};
    lambda0 = [866.963;650.251;433.556;371.650;906.830;840.407;845.665]*1E-6;
    thetab = [17.8;13.1;8.7;7.7;18.6;17.3;17.4]; % Beam sizes (FWHM) in arcsec.
    LineFlux_Kkmpers = [3.5;5.4;7.3;8.1;0.96;0.37;0.21]; % Line fluxes in K.km/s.
    % Conversion to SI units.
    k = 1.38065E-23; % Boltzmann constant in J/K.
    Int = (2E3*k*LineFlux_Kkmpers)./(lambda0.^3); % Int. or line fluxes in W/m²/sr.
    for it = 1:nSAHAI_LineFluxes
        SAHAI_LineFluxes(1+it,:) = {Instru{it},Line{it},lambda0(it)*1e6,...
            thetab(it),'Carina','Flux',Int(it),20};
    end
    save('SAHAI_LineFluxes.mat','SAHAI_LineFluxes')
else
    load('SAHAI_LineFluxes.mat')
end

end